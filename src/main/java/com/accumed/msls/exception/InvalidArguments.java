/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.exception;

/**
 *
 * @author bmahow
 */
public class InvalidArguments extends Exception {

    public InvalidArguments(String message) {
        super(message);
    }
    
}
