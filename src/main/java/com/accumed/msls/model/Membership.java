/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.model;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author smutlak
 */
@XmlRootElement(name = "membership")
@XmlAccessorType(XmlAccessType.FIELD)
public class Membership implements Serializable {
    
    private String sources;
    private String errorMessage;
    private String TransactionDate;
    private String ReferenceNumber;
    private String PolicyID;
    private String EnrollmentDate;
    private String DeletionDate;
    private String UidNumber;
    private String DateOfBirth;
    private String Gender;
    private String PayerID;
    private String PayerName;
    private String FirstName;
    private String SecondName;
    private String FamilyName;

    //extras from payers sites
    private String address;
    private String authNo;
    private String benefits;
    private String copay;
    private String memberId;
    private String message;
    private String packageName;
    private String plan;
    private String tpa;

//    list copay
//            list dedicatbles;
//            list coverages;
    public Membership() {
    }
    
    public Membership(String src, String errorMessage) {
        this.sources = src;
        this.errorMessage = errorMessage;
    }
    
    public String getTransactionDate() {
        return TransactionDate;
    }
    
    public void setTransactionDate(String TransactionDate) {
        this.TransactionDate = TransactionDate;
    }
    
    public String getReferenceNumber() {
        return ReferenceNumber;
    }
    
    public void setReferenceNumber(String ReferenceNumber) {
        this.ReferenceNumber = ReferenceNumber;
    }
    
    public String getPolicyID() {
        return PolicyID;
    }
    
    public void setPolicyID(String PolicyID) {
        this.PolicyID = PolicyID;
    }
    
    public String getEnrollmentDate() {
        return EnrollmentDate;
    }
    
    public void setEnrollmentDate(String EnrollmentDate) {
        this.EnrollmentDate = EnrollmentDate;
    }
    
    public String getDeletionDate() {
        return DeletionDate;
    }
    
    public void setDeletionDate(String DeletionDate) {
        this.DeletionDate = DeletionDate;
    }
    
    public String getUidNumber() {
        return UidNumber;
    }
    
    public void setUidNumber(String UidNumber) {
        this.UidNumber = UidNumber;
    }
    
    public String getDateOfBirth() {
        return DateOfBirth;
    }
    
    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }
    
    public String getGender() {
        return Gender;
    }
    
    public void setGender(String Gender) {
        this.Gender = Gender;
    }
    
    public String getPayerID() {
        return PayerID;
    }
    
    public void setPayerID(String PayerID) {
        this.PayerID = PayerID;
    }
    
    public String getPayerName() {
        return PayerName;
    }
    
    public void setPayerName(String PayerName) {
        this.PayerName = PayerName;
    }
    
    public String getFirstName() {
        return FirstName;
    }
    
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }
    
    public String getSecondName() {
        return SecondName;
    }
    
    public void setSecondName(String SecondName) {
        this.SecondName = SecondName;
    }
    
    public String getFamilyName() {
        return FamilyName;
    }
    
    public void setFamilyName(String FamilyName) {
        this.FamilyName = FamilyName;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }
    
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    public String getSources() {
        return sources;
    }
    
    public void setSources(String sources) {
        this.sources = sources;
    }
    
    public void addSource(String src) {
        if (sources != null) {
            sources += ", "+src;
        } else {
            sources = src;
        }
    }
    
    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getAuthNo() {
        return authNo;
    }
    
    public void setAuthNo(String authNo) {
        this.authNo = authNo;
    }
    
    public String getBenefits() {
        return benefits;
    }
    
    public void setBenefits(String benefits) {
        this.benefits = benefits;
    }
    
    public String getCopay() {
        return copay;
    }
    
    public void setCopay(String copay) {
        this.copay = copay;
    }
    
    public String getMemberId() {
        return memberId;
    }
    
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public String getPackageName() {
        return packageName;
    }
    
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    
    public String getPlan() {
        return plan;
    }
    
    public void setPlan(String plan) {
        this.plan = plan;
    }
    
    public String getTpa() {
        return tpa;
    }
    
    public void setTpa(String tpa) {
        this.tpa = tpa;
    }
    
    public boolean merge(com.accumed.msls.payer.InsuranceCard card, String src) {
        Logger.getLogger(Membership.class.getName()).log(Level.INFO, "merging info from source={0}", src);
        if (this.getPayerID().equals(card.getAuthNo())) {
            this.setDeletionDate(card.getExpiryDate());
            this.setAddress(card.getAddress());
            this.setMemberId(card.getMemberId());
            this.setPlan(card.getPlan());
            this.setBenefits(card.getBenefits());
            this.setMessage(card.getMessage());
            this.addSource(src);
        }
        return true;
    }
    
}
