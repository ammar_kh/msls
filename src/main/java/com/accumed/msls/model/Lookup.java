/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.model;


import com.accumed.msls.dha.DHAClientThread;
import com.accumed.msls.callers.*;


import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author smutlak
 */
@WebService(serviceName = "Lookup")
public class Lookup {

    private static final Services AVAILABLE_SERVICES;
    private static final Hashtable<String, Boolean> blockPayersHashTable = new Hashtable<String, Boolean>();
    private static final Hashtable<String, List<Account>> AdnicFacilityHashTable = new Hashtable<String, List<Account>>();


    static {
        AVAILABLE_SERVICES = new Services();
        AVAILABLE_SERVICES.addService("DHA");
        AVAILABLE_SERVICES.addService("ALMADALLAH");
        AVAILABLE_SERVICES.addService("DAMAN");
        AVAILABLE_SERVICES.addService("NAS");
        AVAILABLE_SERVICES.addService("THIQA");
        AVAILABLE_SERVICES.addService("ALBUHAIRA");
        AVAILABLE_SERVICES.addService("NEXTCARE");
        AVAILABLE_SERVICES.addService("AAFIYA");
        AVAILABLE_SERVICES.addService("WHEALTH");
        AVAILABLE_SERVICES.addService("ADNIC");
        AVAILABLE_SERVICES.addService("NEURON");
    }

    public Lookup() {

    }

    @WebMethod(operationName = "getFacilities")
    public String[] getFacilities() {
        return Utils.getFacilities().toArray(new String[0]);
    }

    @WebMethod(operationName = "getFacilityServices")
    public String[] getFacilityServices(
            @WebParam(name = "facilityLicense") @XmlElement(required = true, nillable = false) String facilityLicense) {
        return Utils.getFacilitiesServives(facilityLicense).toArray(new String[0]);
    }

    @WebMethod(operationName = "getServices")
    public Services getServices() {
        return AVAILABLE_SERVICES;
    }

    /**
     * @param facilityLicense
     * @param emiratesId
     * @param OP
     * @param services
     * @return
     */
    @WebMethod(operationName = "lookupEID")
    public Memberships lookupEID(
            @WebParam(name = "facilityLicense") @XmlElement(required = true, nillable = false) String facilityLicense,
            @WebParam(name = "emiratesId") @XmlElement(required = true, nillable = false) String emiratesId,
            @WebParam(name = "OP") @XmlElement(required = true, nillable = false) Boolean OP,
            @WebParam(name = "services") @XmlElement(required = false, nillable = true) Services services) {

        Logger.getLogger(Lookup.class.getName()).log(Level.INFO, "lookupEID({0}, {1})", new Object[]{emiratesId, OP});
        boolean finshed = false;
        Memberships memberships = new Memberships();
        ConcurrentHashMap<String, Boolean> serviceWithNoAccount = new ConcurrentHashMap();


        if (services == null || services.size() <= 0) {
            services = AVAILABLE_SERVICES;
        }

        ThreadPoolExecutor excutor = new ThreadPoolExecutor(services.size(), services.size(), 180, TimeUnit.SECONDS, new SynchronousQueue<Runnable>());


        DHAClientThread dha = null;
        Account dha_account = null;
        if (services.exists("DHA")) {
            if ((dha_account = Utils.getFacilityAccount(facilityLicense, "DHA")) != null) {
                dha = new DHAClientThread(dha_account, emiratesId);
            } else {
                serviceWithNoAccount.put("DHA", Boolean.FALSE);
            }
        }
        AlmadallahClientThread almadallah = null;

        Account almadallah_account = null;
        if (services.exists("ALMADALLAH")) {
            if ((almadallah_account = Utils.getFacilityAccount(facilityLicense, "ALMADALLAH")) != null) {
                almadallah = new AlmadallahClientThread(almadallah_account, OP ? "OP" : "IP", emiratesId);
            } else {
                serviceWithNoAccount.put("ALMADALLAH", Boolean.FALSE);
            }
        }

        DamanClientThread daman = null;
        Account daman_account = null;

        if (services.exists("DAMAN")) {
            if ((daman_account = Utils.getFacilityAccount(facilityLicense, "DAMAN")) != null) {
                daman = new DamanClientThread(daman_account, OP ? "OP" : "IP", emiratesId);
            } else {
                serviceWithNoAccount.put("DAMAN", Boolean.FALSE);
            }
        }


        NasClientThread nas = null;
        Account nas_account = null;
        if (services.exists("NAS")) {
            if ((nas_account = Utils.getFacilityAccount(facilityLicense, "NAS")) != null) {
                nas = new NasClientThread(nas_account, OP ? "OP" : "IP", emiratesId, "NAS");
            } else {
                serviceWithNoAccount.put("NAS", Boolean.FALSE);
            }
        }

        NeuronClientThread nur = null;
        Account nur_account = null;
        if (services.exists("NEURON")) {
            if ((nur_account = Utils.getFacilityAccount(facilityLicense, "NEURON")) != null) {
                nur = new NeuronClientThread(nur_account, OP ? "OP" : "IP", emiratesId);
            } else {
                serviceWithNoAccount.put("NEURON", Boolean.FALSE);
            }
        }

        ThiqaClientThread thiqa = null;
        Account thiqa_account = null;
        if (services.exists("THIQA")) {
            if ((thiqa_account = Utils.getFacilityAccount(facilityLicense, "THIQA")) != null) {
                thiqa = new ThiqaClientThread(thiqa_account, OP ? "OP" : "IP", emiratesId);
            } else {
                serviceWithNoAccount.put("THIQA", Boolean.FALSE);
            }
        }

        AlbuhairaClientThread albuhaira = null;
        Account albuhaira_account = null;
        if (services.exists("ALBUHAIRA")) {
            if ((albuhaira_account = Utils.getFacilityAccount(facilityLicense, "ALBUHAIRA")) != null) {
                albuhaira = new AlbuhairaClientThread(albuhaira_account, OP ? "OP" : "IP", emiratesId);
            } else {
                serviceWithNoAccount.put("ALBUHAIRA", Boolean.FALSE);
            }
        }


        NextcareClientThread nextcare = null;
        Account nextcare_account = null;
        if (services.exists("NEXTCARE")) {
            if ((nextcare_account = Utils.getFacilityAccount(facilityLicense, "NEXTCARE")) != null) {
                nextcare = new NextcareClientThread(nextcare_account, OP ? "OP" : "IP", emiratesId);
            } else {
                serviceWithNoAccount.put("NEXTCARE", Boolean.FALSE);
            }
        }

        AafiyaClientThread aafiya = null;
        Account aafiya_account = null;
        if (services.exists("AAFIYA")) {
            if ((aafiya_account = Utils.getFacilityAccount(facilityLicense, "AAFIYA")) != null) {
                aafiya = new AafiyaClientThread(aafiya_account, OP ? "OP" : "IP", emiratesId);
            } else {
                serviceWithNoAccount.put("AAFIYA", Boolean.FALSE);
            }
        }
        WhealthClientThread whealth = null;
        Account whealth_account = null;
        if (services.exists("WHEALTH")) {
            if ((whealth_account = Utils.getFacilityAccount(facilityLicense, "WHEALTH")) != null) {
                whealth = new WhealthClientThread(whealth_account, OP ? "OP" : "IP", emiratesId);
            } else {
                serviceWithNoAccount.put("WHEALTH", Boolean.FALSE);
            }
        }

        AdnicClientThread adnic = null;
        Account adnic_account = null;
        if (services.exists("ADNIC")) {
            if ((adnic_account = Utils.getFacilityAccount(facilityLicense, "ADNIC")) != null) {
                adnic = new AdnicClientThread(adnic_account, OP ? "OP" : "IP", emiratesId);
            } else {
                serviceWithNoAccount.put("ADNIC", Boolean.FALSE);
            }
        }


        if (almadallah != null) {         //2
            excutor.submit(almadallah);
        }
        if (daman != null) {              //3
            excutor.submit(daman);
        }
        if (nas != null) {                //4
            excutor.submit(nas);
        }

        if (nur != null) {
            excutor.submit(nur);
        }

        if (nextcare != null) {           //6
            excutor.submit(nextcare);
        }
        if (thiqa != null) {              //7
            excutor.submit(thiqa);
        }

        if (adnic != null) {               //9
            excutor.submit(adnic);
        }
        if (aafiya != null) {             //10
            excutor.submit(aafiya);
        }


        if (albuhaira != null) {          //11
            excutor.submit(albuhaira);
        }


        if (whealth != null) {     //16
            excutor.submit(whealth);
        }


        excutor.shutdown();

        try {
            finshed = excutor.awaitTermination(3, TimeUnit.MINUTES);
        } catch (InterruptedException ex) {
            Logger.getLogger(Lookup.class.getName()).log(Level.SEVERE, null, ex);
        }


        if (serviceWithNoAccount.get("ALMADALLAH") != null) {
            memberships.add(new Membership("ALMADALLAH", "Login account not found!!"));
        } else {
            if (almadallah != null) {         //2
                if (almadallah.getCard() != null) {
                    memberships.add(almadallah.getCard(), "ALMADALLAH");
                }
            }
        }


        if (serviceWithNoAccount.get("DAMAN") != null) {
            memberships.add(new Membership("DAMAN", "Login account not found!!"));
        } else {
            if (daman != null) {              //3
                if (daman.getCard() != null) {
                    memberships.add(daman.getCard(), "DAMAN");
                }
            }
        }


        if (serviceWithNoAccount.get("NAS") != null) {
            memberships.add(new Membership("NAS", "Login account not found!!"));
        } else {
            if (nas != null) {                //4
                if (nas.getCard() != null) {
                    memberships.add(nas.getCard(), "NAS");
                }
            }
        }

        if (serviceWithNoAccount.get("NEURON") != null) {
            memberships.add(new Membership("NEURON", "Login account not found!!"));
        } else {
            if (nur != null) {
                if (nur.getCard() != null) {
                    memberships.add(nur.getCard(), "NEURON");
                }
            }
        }


        if (serviceWithNoAccount.get("THIQA") != null) {
            memberships.add(new Membership("THIQA", "Login account not found!!"));
        } else {
            if (thiqa != null) {              //6
                if (thiqa.getCard() != null) {
                    memberships.add(thiqa.getCard(), "THIQA");
                }
            }
        }


        if (serviceWithNoAccount.get("ALBUHAIRA") != null) {
            memberships.add(new Membership("ALBUHAIRA", "Login account not found!!"));
        } else {
            if (albuhaira != null) {              //9
                if (albuhaira.getCard() != null) {
                    memberships.add(albuhaira.getCard(), "ALBUHAIRA");
                }
            }
        }


        if (serviceWithNoAccount.get("NEXTCARE") != null) {
            memberships.add(new Membership("NEXTCARE", "Login account not found!!"));
        } else {
            if (nextcare != null) {              //11
                if (nextcare.getCard() != null) {
                    memberships.add(nextcare.getCard(), "NEXTCARE");
                }
            }
        }


        if (serviceWithNoAccount.get("AAFIYA") != null) {
            memberships.add(new Membership("AAFIYA", "Login account not found!!"));
        } else {
            if (aafiya != null) {              //10
                if (aafiya.getCard() != null) {
                    memberships.add(aafiya.getCard(), "AAFIYA");
                }
            }
        }


        if (serviceWithNoAccount.get("WHEALTH") != null) {
            memberships.add(new Membership("WHEALTH", "Login account not found!!"));
        } else {
            if (whealth != null) {              //10
                if (whealth.getCard() != null) {
                    memberships.add(whealth.getCard(), "WHEALTH");
                }
            }
        }


        if (serviceWithNoAccount.get("ADNIC") != null) {
            memberships.add(new Membership("ADNIC", "Login account not found!!"));
        } else {
            if (adnic != null) {              //10
                if (adnic.getCard() != null) {
                    memberships.add(adnic.getCard(), "ADNIC");
                }
            }
        }

        //end of working on response.
        Logger.getLogger(Lookup.class.getName()).log(Level.INFO, "END lookupEID({0}, {1})", new Object[]{emiratesId, OP});
        memberships.setEid(emiratesId);
        return memberships;
    }
}
