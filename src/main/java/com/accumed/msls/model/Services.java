/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author smutlak
 */
@XmlRootElement(name = "services")
@XmlAccessorType(XmlAccessType.FIELD)
public class Services implements Serializable {

    private List<Service> service;

    public Services() {
        service = new ArrayList();
    }

    public Services(List<Service> services) {
        this.service = services;
    }

    public boolean addService(String name) {
        this.service.add(new Service(name));
        return true;
    }

    public int size() {
        return this.service.size();
    }

    public boolean exists(String name) {
        for (Service val : service) {
            if (val.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    public List<Service> getServices() {
        return service;
    }

    public void setServices(List<Service> services) {
        this.service = services;
    }

}
