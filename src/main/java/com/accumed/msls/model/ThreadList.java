package com.accumed.msls.model;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ThreadList<T> implements Collection<Thread> {
    List<Thread> threads = new ArrayList<>();

    @Override
    public int size() {
        return threads.size();
    }

    @Override
    public boolean isEmpty() {
        return threads.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @NotNull
    @Override
    public Iterator<java.lang.Thread> iterator() {
        return null;
    }

    @NotNull
    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] a) {
        return null;
    }

    @Override
    public boolean add(Thread thread) {
        return threads.add(thread);
    }

    @Override
    public boolean remove(Object o) {
        return threads.remove(o);
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends java.lang.Thread> c) {
        return threads.addAll(c);
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> c) {
        return threads.removeAll(c);
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        threads.clear();
    }

    public T get(int index) {
        return (T) threads.get(index);
    }

    public int aliveCount() {
        int count = 0;
        for (Thread t : threads) {
            if (t.isAlive())
                count++;
        }
        return count;
    }

    public T getIdle(){
        for(Thread t: threads){
            if(!t.isAlive()){
                return (T) t;
            }
        }
        return null;
    }

    public int getIdleCount(){
        int count = 0;
        for (Thread t : threads) {
            if (!t.isAlive())
                count++;
        }
        return count;
    }
}
