/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author smutlak
 */
@XmlRootElement(name = "memberships")
@XmlAccessorType(XmlAccessType.FIELD)
public class Memberships implements Serializable {

    private String eid;
    private List<Membership> memberships;

    public Memberships() {
        memberships = new ArrayList();
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public List<Membership> getMemberships() {
        return memberships;
    }

    public void setMemberships(List<Membership> memberships) {
        this.memberships = memberships;
    }

    public boolean add(Membership newMembership) {
        this.memberships.add(newMembership);
        return true;
    }

    public boolean addAll(List<Membership> newMemberships) {
        this.memberships.addAll(newMemberships);
        return true;
    }

    public boolean add(com.accumed.msls.dha.Members members, String src) {
        if (members.getErrorMsg() != null && members.getErrorMsg().length() > 0) {
            this.memberships.add(new Membership(src, members.getErrorMsg()));
            return true;
        }
        for (com.accumed.msls.dha.Members.Member mem : members.getMembers()) {
            {//  com.accumed.msls.dha.Members.Member mem = members.getMember();
                Membership newMem = new Membership();
                newMem.addSource(src);
                newMem.setDateOfBirth(mem.getDateOfBirth());
                newMem.setDeletionDate(mem.getDeletionDate());
                newMem.setEnrollmentDate(mem.getEnrollmentDate());
                newMem.setFamilyName(mem.getFamilyName());
                newMem.setFirstName(mem.getFirstName());
                newMem.setGender(mem.getGender());
                newMem.setPayerID(mem.getPayerID());
                newMem.setPayerName(mem.getPayerName());
                newMem.setPolicyID(mem.getPolicyID());
                newMem.setReferenceNumber(mem.getReferenceNumber());
                newMem.setSecondName(mem.getSecondName());
                newMem.setTransactionDate(mem.getTransactionDate());
                newMem.setUidNumber(mem.getUidNumber());
                this.memberships.add(newMem);
            }
        }
        return true;
    }

    public boolean add(com.accumed.ws.wsinterface.msls.Memberships members) {
        for (com.accumed.ws.wsinterface.msls.Membership mem : members.getMemberships()) {
            {
                if (mem.getErrorMessage() != null && mem.getErrorMessage().length() > 0) {
                    this.memberships.add(new Membership(mem.getSources(), mem.getErrorMessage()));
                    continue;
                } else {
                    Membership newMem = new Membership();
                    newMem.addSource(mem.getSources());
                    newMem.setDateOfBirth(mem.getDateOfBirth());
                    newMem.setDeletionDate(mem.getDeletionDate());
                    newMem.setEnrollmentDate(mem.getEnrollmentDate());
                    newMem.setFamilyName(mem.getFamilyName());
                    newMem.setFirstName(mem.getFirstName());
                    newMem.setGender(mem.getGender());
                    newMem.setPayerID(mem.getPayerID());
                    newMem.setPayerName(mem.getPayerName());
                    newMem.setPolicyID(mem.getPolicyID());
                    newMem.setReferenceNumber(mem.getReferenceNumber());
                    newMem.setSecondName(mem.getFamilyName());
                    newMem.setTransactionDate(mem.getTransactionDate());
                    newMem.setUidNumber(mem.getUidNumber());
                    newMem.setAddress(mem.getAddress());
                    newMem.setAuthNo(mem.getAuthNo());
                    newMem.setBenefits(mem.getBenefits());
                    newMem.setCopay(mem.getCopay());
                    newMem.setMemberId(mem.getMemberId());
                    newMem.setMessage(mem.getMessage());
                    newMem.setPackageName(mem.getPackageName());
                    newMem.setPlan(mem.getPlan());
                    newMem.setTpa(mem.getTpa());
                    this.memberships.add(newMem);
                }
            }
        }
        return true;
    }

    public boolean add(com.accumed.msls.payer.InsuranceCard card, String src) {
        if (card != null && card.getSuccess() > 0) {
            Membership newMem = new Membership();
            newMem.addSource(src);
            newMem.setDateOfBirth(card.getDateOfBirth());
            newMem.setDeletionDate(card.getExpiryDate());
            newMem.setEnrollmentDate(card.getStartDate());
            newMem.setFamilyName(card.getName());
            newMem.setFirstName(card.getName());
            newMem.setGender(card.getGender());
            newMem.setPayerID(card.getInsurers());
            newMem.setPayerName(card.getPayer());
            newMem.setPolicyID("");
            newMem.setReferenceNumber("");
            newMem.setSecondName(card.getName());
            newMem.setTransactionDate("");
            newMem.setUidNumber("");
            newMem.setAddress(card.getAddress());
            newMem.setAuthNo(card.getAuthNo());
            newMem.setBenefits(card.getBenefits());
            newMem.setCopay(card.getCopay());
            newMem.setMemberId(card.getMemberId());
            newMem.setMessage(card.getMessage());
            newMem.setPackageName(card.getPackageName());
            newMem.setPlan(card.getPlan());
            newMem.setTpa(card.getTpa());

            this.memberships.add(newMem);
        } else if (card != null) {
            Membership newMem = new Membership();
            newMem.addSource(src);
            newMem.setErrorMessage("(" + card.getSuccess() + ") " + card.getMessage());
            this.memberships.add(newMem);
        }

        return true;
    }
}
