/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.model;

import com.accumed.msls.exception.HtmlParsingException;
import com.accumed.msls.exception.InvalidArguments;
import com.accumed.msls.exception.TimeoutException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.html.FrameWindow;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableBody;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author smutlak
 */
public class Utils {

    static List<String> patterns = new ArrayList<String>();
    private static final List<Account> FACILITY_ACCOUNTS = new ArrayList();

    protected static Set<String> getFacilitiesServives(String license) {
        Set<String> ret = new HashSet<>();

        if (FACILITY_ACCOUNTS.isEmpty()) {
            readFacilityAccounts();
        }

        if (!FACILITY_ACCOUNTS.isEmpty()) {
            for (Account account : FACILITY_ACCOUNTS) {
                if (account.getFacilityLicense().equalsIgnoreCase(license)) {
                    ret.add(account.getService() + "\t" + account.isEnabled());
                }
            }
        }
        return ret;
    }

    protected static Set<String> getFacilities() {
        Set<String> ret = new HashSet<>();

        if (FACILITY_ACCOUNTS.isEmpty()) {
            readFacilityAccounts();
        }

        if (!FACILITY_ACCOUNTS.isEmpty()) {
            for (Account account : FACILITY_ACCOUNTS) {
                ret.add(account.getFacilityLicense());
            }
        }
        return ret;
    }

    public static Account getFacilityAccount(String facilityLicense, String service) {
        if (FACILITY_ACCOUNTS.isEmpty()) {
            readFacilityAccounts();
        }
        for (Account acc : FACILITY_ACCOUNTS) {
            if (acc.getFacilityLicense().equalsIgnoreCase(facilityLicense)
                    && acc.getService().equalsIgnoreCase(service) && acc.isEnabled()) {
                return acc;
            }
        }
        return null;
    }

    public static List<Account> getFacilitiesAccount(String facilityLicense, String service) {
        if (FACILITY_ACCOUNTS.isEmpty()) {
            readFacilityAccounts();
        }
        List<Account> accountsList = new ArrayList<Account>();
        for (Account acc : FACILITY_ACCOUNTS) {
            if (acc.getFacilityLicense().equalsIgnoreCase(facilityLicense)
                    && acc.getService().equalsIgnoreCase(service) && acc.isEnabled()) {
                accountsList.add(acc);
            }
        }
        if (accountsList.size() != 0) {
            return accountsList;
        } else {
            return null;
        }
    }

    private static void readFacilityAccounts() {
        java.io.File csvFile = new java.io.File("./bs_facility_scrubbing_accounts.csv");
        if (csvFile.exists()) {
            java.io.FileReader filereader = null;
            try {
                filereader = new java.io.FileReader(csvFile);
                com.opencsv.CSVParser csvParser = new com.opencsv.CSVParserBuilder()
                        .withSeparator('\t')
                        .build();
                com.opencsv.CSVReader csvReader = new com.opencsv.CSVReaderBuilder(filereader)
                        .withCSVParser(csvParser)
                        .withSkipLines(1)
                        .build();
                java.util.List<String[]> allData = csvReader.readAll();

                // print Data 
                for (String[] row : allData) {
                    String service = row[0].replaceAll("(^\\h*)|(\\h*$)", "").trim();
                    String facilityLicense = row[1].replaceAll("(^\\h*)|(\\h*$)", "").trim();
                    String userName = row[2].replaceAll("(^\\h*)|(\\h*$)", "").trim();
                    String password = row[3].replaceAll("(^\\h*)|(\\h*$)", "").trim();
                    String enabled = row[4].replaceAll("(^\\h*)|(\\h*$)", "").trim();
                    System.out.println("account DATA: " + service + " " + facilityLicense + " " + userName + " " + password + " " + enabled);
                    try {
                        FACILITY_ACCOUNTS.add(new Account(facilityLicense, service, userName, password, Boolean.parseBoolean(enabled)));
                    } catch (NumberFormatException e) {
                        Logger.getLogger(Lookup.class.getName()).log(Level.SEVERE, null, e);
                    }
                }

            } catch (FileNotFoundException ex) {
                Logger.getLogger(Lookup.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Lookup.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (filereader != null) {
                        filereader.close();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Lookup.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else { //if the file is not exists --create it
            Logger.getLogger(Lookup.class.getName()).severe("accounts file was not found creating new one.");
            try {
                csvFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static HtmlPage reloadPageAfterSubmission(String serviceName, WebWindow window, HtmlPage page) throws InterruptedException, NamingException, TimeoutException {
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        int spentTime = 0;
        int step = 500;
        while (window.getEnclosedPage() == page) {
            // The page hasn't changed.
            if (spentTime > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }
            Thread.sleep(step);
            spentTime += step;
        }
        // This loop above will wait until the page changes.
        return (HtmlPage) window.getEnclosedPage();
    }

    public static boolean isContains2Dates(String date) {
        int count = 0;
        if (date != null) {
            for (int i = 0; i < date.length(); i++) {
                if (Character.isDigit(date.charAt(i))) {
                    count++;
                }
            }
        }
        return date != null && count > 8;
    }

    public static String formatDate(Date date, String pattern) {
        try {
            return new SimpleDateFormat(pattern).format(date);
        } catch (Exception ex) {
//            Logger.getLogger(ClaimController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Date parseStartDate(String date) {
        Date returnedDate = null;
        int count = 0;
        if (date != null) {
            for (int i = 0; i < date.length(); i++) {
                if (Character.isDigit(date.charAt(i))) {
                    count++;
                }
            }
        }
        if (date != null && count <= 8) {
            returnedDate = parseDate(date.trim());
        } else if (date != null && count > 8) {
            try {
                String[] dateArray = date.split("\\-");
                if (dateArray.length <= 1) {
                    dateArray = date.split("and");
                }
                if (dateArray.length == 2) {
                    returnedDate = parseDate(dateArray[0]);
                }
            } catch (Exception e) {
            }
        }
        return returnedDate;
    }

    public static Date parseDate(String date) {
        if (patterns.isEmpty()) {
            patterns.add("d/MM/yyyy");
            patterns.add("dd/MM/yyyy");
            patterns.add("d/MMM/yyyy");
            patterns.add("dd/MMM/yyyy");
            patterns.add("d-MM-yyyy");
            patterns.add("dd-MM-yyyy");
            patterns.add("d-MMM-yyyy");
            patterns.add("dd-MMM-yyyy");
            patterns.add("d MM yyyy");
            patterns.add("dd MM yyyy");
            patterns.add("d MMM yyyy");
            patterns.add("dd MMM yyyy");
        }
        Date returnedDate = null;

        if (date != null) {
            for (String pattern : patterns) {
                returnedDate = parseDateWithPattern(date, pattern);
                if (returnedDate != null) {
                    break;
                }
            }
        }

        return returnedDate;
    }

    public static Date parseDateWithPattern(String date, String pattern) {
        try {
            return new SimpleDateFormat(pattern).parse(date.trim());
        } catch (ParseException ex) {
//            Logger.getLogger(ClaimController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Date parseExpiryDate(String date) {
        Date returnedDate = null;
        int count = 0;
        if (date != null) {
            for (int i = 0; i < date.length(); i++) {
                if (Character.isDigit(date.charAt(i))) {
                    count++;
                }
            }
        }
        if (date != null && count <= 8) {
            returnedDate = parseDate(date.trim());
        } else if (date != null && count > 8) {
            try {
                String[] dateArray = date.split("\\-");
                if (dateArray.length <= 1) {
                    dateArray = date.split("and");
                }
                if (dateArray.length == 2) {
                    returnedDate = parseDate(dateArray[1]);
                }
            } catch (Exception e) {
            }
        }
        return returnedDate;
    }

    public static boolean startsWithIgnoreCase(String s1, String s2) {
        boolean ret;
        ret = s1.toLowerCase().startsWith(s2.toLowerCase());
        System.out.println("startsWithIgnoreCase(" + s1 + "," + s2 + ") is=" + ret);
        return ret;
    }

    public static boolean containIgnoreCase(String s1, String s2) {
        boolean ret;
        ret = s1.toLowerCase().contains(s2.toLowerCase());
        System.out.println("startsWithIgnoreCase(" + s1 + "," + s2 + ") is=" + ret);
        return ret;
    }

    public static HtmlPage isLoadedPage(String serviceName, WebClient WEB_CLIENT, String pageUrl)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || pageUrl == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (pageUrl == null) {
                exceptionMsg += "pageUrl is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }

        HtmlPage ret;
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            try {
                ret = WEB_CLIENT.getPage(pageUrl);
            } catch (com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException e) {
                e.printStackTrace();
                if (e.getMessage().startsWith("500")) {
                    throw new HtmlParsingException(serviceName + ", " + e.getMessage());
                }
                throw e;
            }
            if (ret != null && startsWithIgnoreCase(pageUrl, ret.getUrl().toString())) {
                return ret;
            }

            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }

            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, ret);
            realTimeWait += timeWait;
        }
        return null;
    }

    public static HtmlElement isLoadedElement(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage, String xPath)
            throws Exception {
        return isLoadedElement(serviceName, WEB_CLIENT, htmlPage, xPath, null);
    }

    public static HtmlElement isLoadedElement(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage, String xPath, String xPath2)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || htmlPage == null || xPath == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (htmlPage == null) {
                exceptionMsg += "htmlPage is null, ";
            }
            if (xPath == null) {
                exceptionMsg += "xPath is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }

        HtmlElement ret;
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        System.out.println(serviceName + " timeout=" + timeOut);
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            ret = htmlPage.getFirstByXPath(xPath);
            if (ret == null && xPath2 != null) {
                ret = htmlPage.getFirstByXPath(xPath2);
            }
            if (ret != null) {
                if (ret instanceof HtmlTable) {
                    HtmlTable table = (HtmlTable) ret;
                    List<HtmlTableRow> rows = table.getRows();
                    if (rows.get(0) != null && rows.get(0).getCell(1) != null && !rows.get(0).getCell(1).asText().trim().isEmpty()) {
                        return ret;
                    }
                    if (realTimeWait > timeOut) {
                        throw new TimeoutException("Service Name = " + serviceName);
                    }
                    long timeWait = getPreferedWaitTime(i, max_i);
                    wait_(WEB_CLIENT, timeWait, htmlPage);
                    realTimeWait += timeWait;
                } else {
                    return ret;
                }
            }

            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }

            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, htmlPage);
            realTimeWait += timeWait;
        }
        return null;
    }

    public static HtmlElement isLoadedElementOfNeuron(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage, String xPath, String xPath2)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || htmlPage == null || xPath == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (htmlPage == null) {
                exceptionMsg += "htmlPage is null, ";
            }
            if (xPath == null) {
                exceptionMsg += "xPath is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }

        HtmlElement ret;
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        System.out.println(serviceName + " timeout=" + timeOut);
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            ret = htmlPage.getFirstByXPath(xPath);
            if (ret == null && xPath2 != null) {
                ret = htmlPage.getFirstByXPath(xPath2);
            }
            if (ret != null) {
                if (ret instanceof HtmlTable) {
                    HtmlTable table = (HtmlTable) ret;
                    List<HtmlTableRow> rows = table.getRows();
                    if (rows.size() > 1) {
                        if (rows.get(1) != null && rows.get(1).getCell(1) != null && !rows.get(1).getCell(1).asText().trim().isEmpty()) {
                            return ret;
                        }
                    }

                    if (realTimeWait > timeOut) {
                        throw new TimeoutException("Service Name = " + serviceName);
                    }
                    long timeWait = getPreferedWaitTime(i, max_i);
                    wait_(WEB_CLIENT, timeWait, htmlPage);
                    realTimeWait += timeWait;
                } else {
                    return ret;
                }
            }

            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }

            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, htmlPage);
            realTimeWait += timeWait;
        }
        return null;
    }

    public static HtmlElement isLoadedContentOfElement(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage, String xPath, String xPath2)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || htmlPage == null || xPath == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (htmlPage == null) {
                exceptionMsg += "htmlPage is null, ";
            }
            if (xPath == null) {
                exceptionMsg += "xPath is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }

        HtmlElement ret;
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        System.out.println(serviceName + " timeout=" + timeOut);
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            ret = htmlPage.getFirstByXPath(xPath);
            if (ret == null && xPath2 != null) {
                ret = htmlPage.getFirstByXPath(xPath2);
            }
            if (ret != null && !ret.asText().isEmpty()) {
                if (ret instanceof HtmlTable) {
                    HtmlTable table = (HtmlTable) ret;
                    List<HtmlTableRow> rows = table.getRows();
                    if (rows.get(0) != null && rows.get(0).getCell(1) != null && !rows.get(0).getCell(1).asText().trim().isEmpty()) {
                        return ret;
                    }
                    if (realTimeWait > timeOut) {
                        throw new TimeoutException("Service Name = " + serviceName);
                    }
                    long timeWait = getPreferedWaitTime(i, max_i);
                    wait_(WEB_CLIENT, timeWait, htmlPage);
                    realTimeWait += timeWait;

                } else {
                    return ret;
                }
            }

            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }

            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, htmlPage);
            realTimeWait += timeWait;
        }
        return null;
    }

    public static HtmlElement isLoadedContentOfElementOneIsButton(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage, String xPath, String xPath2)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || htmlPage == null || xPath == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (htmlPage == null) {
                exceptionMsg += "htmlPage is null, ";
            }
            if (xPath == null) {
                exceptionMsg += "xPath is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }

        HtmlElement ret;
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        System.out.println(serviceName + " timeout=" + timeOut);
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            ret = htmlPage.getFirstByXPath(xPath);
            if ((ret == null || ret.asText().isEmpty()) && xPath2 != null) {
                ret = htmlPage.getFirstByXPath(xPath2);
            }
            if (ret != null && !ret.asText().isEmpty()) {
                if (ret instanceof HtmlTable) {
                    HtmlTable table = (HtmlTable) ret;
                    List<HtmlTableRow> rows = table.getRows();
                    if (rows.get(0) != null && rows.get(0).getCell(1) != null && !rows.get(0).getCell(1).asText().trim().isEmpty()) {
                        return ret;
                    }
                    if (realTimeWait > timeOut) {
                        throw new TimeoutException("Service Name = " + serviceName);
                    }
                    long timeWait = getPreferedWaitTime(i, max_i);
                    wait_(WEB_CLIENT, timeWait, htmlPage);
                    realTimeWait += timeWait;

                } else {
                    return ret;
                }
            }

            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }

            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, htmlPage);
            htmlPage = (HtmlPage) WEB_CLIENT.getCurrentWindow().getEnclosedPage();
            realTimeWait += timeWait;
        }
        return null;
    }

    public static HtmlElement isLoadedValueOfElement(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage, String xPath)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || htmlPage == null || xPath == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (htmlPage == null) {
                exceptionMsg += "htmlPage is null, ";
            }
            if (xPath == null) {
                exceptionMsg += "xPath is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }

        HtmlInput ret;
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        System.out.println(serviceName + " timeout=" + timeOut);
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            ret = htmlPage.getFirstByXPath(xPath);
            if (ret != null && ret.getValueAttribute() != null && !ret.getValueAttribute().isEmpty()) {
                return ret;
            }

            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }

            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, htmlPage);
            realTimeWait += timeWait;
            htmlPage = (HtmlPage) WEB_CLIENT.getCurrentWindow().getEnclosedPage();
            System.out.println("URL : " + htmlPage.getUrl());
        }
        return null;
    }

    public static HtmlElement isLoadedOptionsOfElement(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage, String xPath, String xPath2)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || htmlPage == null || xPath == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (htmlPage == null) {
                exceptionMsg += "htmlPage is null, ";
            }
            if (xPath == null) {
                exceptionMsg += "xPath is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }

        HtmlElement ret;
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        System.out.println(serviceName + " timeout=" + timeOut);
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            ret = htmlPage.getFirstByXPath(xPath);
            if ((ret == null || (ret instanceof HtmlSelect && ((HtmlSelect) ret).getOptions().size() == 0)) && xPath2 != null) {
                ret = htmlPage.getFirstByXPath(xPath2);
            }
            if (ret != null && !ret.getTextContent().isEmpty()) {
                if (ret instanceof HtmlSelect) {
                    HtmlSelect hs = (HtmlSelect) ret;

                    if (hs.getOptions() != null && hs.getOptions().size() > 1) {
                        return ret;
                    }
                    if (realTimeWait > timeOut) {
                        throw new TimeoutException("Service Name = " + serviceName);
                    }
                    long timeWait = getPreferedWaitTime(i, max_i);
                    wait_(WEB_CLIENT, timeWait, htmlPage);
                    realTimeWait += timeWait;
                    htmlPage = (HtmlPage) WEB_CLIENT.getCurrentWindow().getEnclosedPage();

                } else {
                    return ret;
                }
            }

            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }

            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, htmlPage);
            realTimeWait += timeWait;
            htmlPage = (HtmlPage) WEB_CLIENT.getCurrentWindow().getEnclosedPage();
        }
        return null;
    }

    public static HtmlTableBody isLoadedHtmlTableBody(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage, String xPath)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || htmlPage == null || xPath == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (htmlPage == null) {
                exceptionMsg += "htmlPage is null, ";
            }
            if (xPath == null) {
                exceptionMsg += "xPath is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }
        HtmlTableBody ret = null;
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            HtmlTable htmlTable = (HtmlTable) htmlPage.getByXPath(xPath).get(0);
            if (htmlTable != null) {
                ret = ((HtmlTable) htmlPage.getByXPath(xPath).get(0)).getBodies().get(0);
                if (ret.getRows() != null && ret.getRows().size() > 0) {
                    if (ret.getRows().get(0).getCells() != null && ret.getRows().get(0).getCells().size() > 1) {
                        if (ret != null && ret.getRows().get(0).getCells().get(1) != null && !ret.getRows().get(0).getCells().get(1).getTextContent().replaceAll("\n", "").trim().isEmpty()) {
                            return ret;
                        }
                    }
                }
            }
            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }
            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, htmlPage);
            realTimeWait += timeWait;

        }
        return null;

    }

    public static HtmlTable isLoadedHtmlTable(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage, String xPath)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || htmlPage == null || xPath == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (htmlPage == null) {
                exceptionMsg += "htmlPage is null, ";
            }
            if (xPath == null) {
                exceptionMsg += "xPath is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }
        HtmlTable ret = null;
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            ret = (HtmlTable) htmlPage.getByXPath(xPath).get(0);
            if (ret != null) {
                HtmlTableBody htmlTableBody = ((HtmlTable) htmlPage.getByXPath(xPath).get(0)).getBodies().get(0);
                if (htmlTableBody != null) {
                    if (htmlTableBody.getRows().get(0).getCells() != null && htmlTableBody.getRows().get(0).getCells().size() > 1) {
                        if (htmlTableBody.getRows().get(0).getCells().get(1) != null && !htmlTableBody.getRows().get(0).getCells().get(1).getTextContent().replaceAll("\n", "").trim().isEmpty()) {
                            return ret;
                        }
                    }
                }

            }
            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }
            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, htmlPage);
            htmlPage = (HtmlPage) WEB_CLIENT.getCurrentWindow().getEnclosedPage();
            realTimeWait += timeWait;

        }
        return null;
    }

    public static List<HtmlElement> isLoadedListOfElement(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage, String xPath)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || htmlPage == null || xPath == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (htmlPage == null) {
                exceptionMsg += "htmlPage is null, ";
            }
            if (xPath == null) {
                exceptionMsg += "xPath is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }

        List<HtmlElement> ret;
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        System.out.println(serviceName + " timeout=" + timeOut);
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            ret = htmlPage.getByXPath(xPath);
            if (ret != null && !ret.isEmpty()) {
                return ret;
            }

            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }

            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, htmlPage);
            realTimeWait += timeWait;
        }
        return null;
    }

    public static List<FrameWindow> isLoadedListOfFrameWindow(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || htmlPage == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (htmlPage == null) {
                exceptionMsg += "htmlPage is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }

        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        List<FrameWindow> ret;
        for (int i = 1; i <= max_i; i++) {
            ret = htmlPage.getFrames();
            if (ret != null && !ret.isEmpty()) {
                return ret;
            }
            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }

            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, htmlPage);
            realTimeWait += timeWait;
        }
        return null;
    }

    public static int getPreferedLoopMax(long timeout) {
        return (new Double(timeout / 375)).intValue();
    }

    public static long getPreferedWaitTime(int counter, int max) {
        double d = (new Double(counter)) / (new Double(max));

        Double ret = (((1 - java.lang.Math.sin(d * 4)) * 500) + 100);
        return ret.longValue();
    }

    public static int getSiteTimeout(String serviceName) throws NamingException {
        int timeWait = 0;
        try {
            String sVarName = serviceName.toUpperCase() + "_TIMEOUT";
            timeWait = Integer.parseInt((String) (new InitialContext().lookup("java:comp/env/" + sVarName)));
        } catch (NumberFormatException | NamingException e) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, "[{0}] not found, using default timeout.", serviceName);
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, e);
            timeWait = Integer.parseInt((String) (new InitialContext().lookup("java:comp/env/GLOBAL_TIMEOUT")));
        }
        return timeWait;
    }

    public static String getUserNameOfService(String serviceName) {
        try {
            String userName = (String) (new InitialContext().lookup("java:comp/env/" + serviceName.toUpperCase() + "_USERNAME"));
            if (!userName.isEmpty()) {
                return userName;
            } else {
                return "";
            }
        } catch (Exception e) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, e);
            return "";
        }

    }

    public static String getPasswordOfService(String serviceName) {
        try {
            String password = (String) (new InitialContext().lookup("java:comp/env/" + serviceName.toUpperCase() + "_PASSWORD"));
            if (!password.isEmpty()) {
                return password;
            } else {
                return "";
            }
        } catch (Exception e) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, e);
            return "";
        }

    }

    private static void wait_(WebClient WEB_CLIENT, long timeWait, HtmlPage obj) throws InterruptedException {
        long ls = System.currentTimeMillis();
        WEB_CLIENT.waitForBackgroundJavaScript(timeWait);
        long le = System.currentTimeMillis();
        if (((le - ls) + 5) < timeWait) {
            synchronized (obj) {
                obj.wait(timeWait);
            }
        }
    }

    /* for pentacare by Bassem*/
    public static boolean isLogin(String serviceName, WebClient WEB_CLIENT, HtmlPage page, String errorMessage, String pageUrl)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || pageUrl == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (page == null) {
                exceptionMsg += "page is null, ";
            }
            if (errorMessage == null) {
                exceptionMsg += "errorMessage is null, ";
            }
            if (pageUrl == null) {
                exceptionMsg += "pageUrl is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }
        int timeOut = getSiteTimeout(serviceName) * 1000;// second 
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            try {
                if (page.asXml().contains(errorMessage)) {
                    return false;
                } else {
                    page = (HtmlPage) WEB_CLIENT.getCurrentWindow().getEnclosedPage();
                }

            } catch (com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException e) {
                e.printStackTrace();
                if (e.getMessage().startsWith("500")) {
                    throw new HtmlParsingException(serviceName + ", " + e.getMessage());
                }
                throw e;
            }
            if (page != null && !startsWithIgnoreCase(pageUrl, page.getUrl().toString())) {
                return true;
            }

            if (realTimeWait > timeOut) {
                throw new TimeoutException("Service Name = " + serviceName);
            }

            long timeWait = getPreferedWaitTime(i, max_i);
            wait_(WEB_CLIENT, timeWait, page);
            realTimeWait += timeWait;
        }
        return false;
    }

    /*end of pentacare from bassem*/

 /*start of axa from bassem*/
//true : loaded Element
    //false: loaded page
    public static boolean isLoadedElementOrPage(String serviceName, WebClient WEB_CLIENT, HtmlPage htmlPage, String xPath, String pageUrl)
            throws Exception {

        if (serviceName == null || WEB_CLIENT == null || htmlPage == null || xPath == null) {
            String exceptionMsg = "";
            if (serviceName == null) {
                exceptionMsg += "service name is null, ";
            }
            if (WEB_CLIENT == null) {
                exceptionMsg += "WEB_CLIENT is null, ";
            }
            if (htmlPage == null) {
                exceptionMsg += "htmlPage is null, ";
            }
            if (xPath == null) {
                exceptionMsg += "xPath is null, ";
            }
            if (pageUrl == null) {
                exceptionMsg += "pageUrl is null, ";
            }
            throw new InvalidArguments(serviceName != null ? "Service=" + serviceName + " " + exceptionMsg
                    : exceptionMsg);
        }

        HtmlElement ret;
        int timeOut = 10000;// second 
        System.out.println(serviceName + " timeout=" + timeOut);
        int max_i = getPreferedLoopMax(timeOut);
        int realTimeWait = 0;
        for (int i = 1; i <= max_i; i++) {
            ret = htmlPage.getFirstByXPath(xPath);
            if (ret != null && !ret.asText().isEmpty()) {
                return true;
            }
            if (htmlPage != null && !startsWithIgnoreCase(pageUrl, htmlPage.getUrl().toString())) {
                return false;
            }

            if (realTimeWait > timeOut) {
                return false;
            }

            long timeWait = getPreferedWaitTime(i, max_i);
            //WEB_CLIENT.waitForBackgroundJavaScript(timeWait);
            wait_(WEB_CLIENT, timeWait, htmlPage);
            htmlPage = (HtmlPage) WEB_CLIENT.getCurrentWindow().getEnclosedPage();
            realTimeWait += timeWait;
        }
        return false;
    }

    /*end of axa from bassem*/
}
