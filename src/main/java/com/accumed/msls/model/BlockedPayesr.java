/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.model;

import com.accumed.ws.wsinterface.msls.LookupProxy;
import com.accumed.ws.wsinterface.msls.Lookup_PortType;
import com.accumed.ws.wsinterface.msls.Service;
import com.accumed.ws.wsinterface.msls.Memberships;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bmahow
 */
public class BlockedPayesr {

    private List<String> payersNameList;
    private String facilityName = "";
    private Service[] srv;
    private String eids;

    private Lookup_PortType soap;
    private LookupProxy proxy;

    public BlockedPayesr() {
        payersNameList = new ArrayList<String>();
    }

    public BlockedPayesr(String facilityLicense, String emiratesId) {
        this.facilityName = facilityLicense;
        this.eids = emiratesId;
        payersNameList = new ArrayList<String>();
    }

    public Memberships lookupEID() {
        Memberships ships = null;
        try {
            proxy = new LookupProxy();
            soap = proxy.getLookup_PortType();
            srv = new Service[payersNameList.size()];
            int count = 0;
            for (String pName : payersNameList) {
                srv[count] = new Service(pName);
                count++;
            }
            try {
                ships = soap.lookupEID(facilityName, eids, true, srv);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.getLogger(BlockedPayesr.class.getName()).log(Level.SEVERE, null, e);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger(BlockedPayesr.class.getName()).log(Level.SEVERE, null, e);
        }

        return ships;
    }

    public void addService(String pNmae) {
        payersNameList.add(pNmae);
    }

    public List<String> getPayersNameList() {
        return payersNameList;
    }

    public void setPayersNameList(List<String> payersNameList) {
        this.payersNameList = payersNameList;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public Service[] getSrv() {
        return srv;
    }

    public void setSrv(Service[] srv) {
        this.srv = srv;
    }

    public String getEids() {
        return eids;
    }

    public void setEids(String eids) {
        this.eids = eids;
    }

    public Lookup_PortType getSoap() {
        return soap;
    }

    public void setSoap(Lookup_PortType soap) {
        this.soap = soap;
    }

    public LookupProxy getProxy() {
        return proxy;
    }

    public void setProxy(LookupProxy proxy) {
        this.proxy = proxy;
    }

}
