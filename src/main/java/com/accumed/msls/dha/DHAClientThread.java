/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.dha;

import com.accumed.msls.model.Account;

import java.io.StringReader;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

/**
 *
 * @author waltasseh
 */
public class DHAClientThread implements Runnable {

    String eid;
    Members members;
    Account account;

    public DHAClientThread(Account account, String eid) {
        this.account = account;
        this.eid = eid;
    }

    @Override
    public void run() {

        Logger.getLogger(DHAClientThread.class.getName()).log(Level.INFO, "{0} Start DHAClientThread CLient ...", new Date());
        try { // Call Web Service Operation
            eclaimlinkSearchIdentity.SearchIdentity service = new eclaimlinkSearchIdentity.SearchIdentity();
            eclaimlinkSearchIdentity.ISearchIdentity port = service.getBasicEndpoint();
            // TODO process result here
            eclaimlinkSearchIdentity.MemberInsuranceResponse response = port.searchEmiratesId(account.getUser(), account.getPass(), eid);
            System.out.println("Result = " + response);
            String errorMessage = "";
            switch (response.getResult()) {
                /*case 1: //Successful, Record found with xml message structure.
                    break;*/
                case -1: //Login failed or error in input fields.
                    errorMessage = "Login information failed.";
                    break;
                case -3: //No policy found for this Emirates ID record.
                    errorMessage = "No policy found for this Emirates ID record.";
                    break;
                case -4: //Unexpected error occurred.
                    errorMessage = "Unexpected error occurred.";
                    break;
                case -5: //Emirates ID match but the policy expired.
                    errorMessage = "Emirates ID match but the policy expired.";
                    break;
            }
            if (response.getResult() != 1) {
                members = new Members();
                members.setErrorMsg(errorMessage);
            } else {
                if (response.getErrorMessage() != null && !response.getErrorMessage().isEmpty()) {
                    members = new Members();
                    members.setErrorMsg(response.getErrorMessage());
                } else if (response.getMemberInformation().startsWith("<Members>")) {
                    members = parseXMLString(response.getMemberInformation());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(DHAClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger(DHAClientThread.class.getName()).log(Level.INFO, "{0} Start DHAClientThread CLient ...", new Date());
    }

    private static Members parseXMLString(String input) {

//            input = input.replaceAll("\"", "&quot;");
//            input = input.replaceAll("'", "&apos;");
//            input = input.replaceAll("<", "&lt;");
//            input = input.replaceAll(">", "&gt;");
        input = input.replaceAll("&", "&amp;");

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Members.class
            );
            javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            StringReader reader = new StringReader(input);
            Members members = (Members) unmarshaller.unmarshal(reader);
            return members;

        } catch (JAXBException ex) {
            Logger.getLogger(DHAClientThread.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public Members getMembers() {
        return members;
    }

}
