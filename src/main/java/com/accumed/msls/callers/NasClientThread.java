/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.callers;

import com.accumed.msls.model.Account;
import com.accumed.msls.model.Utils;
import com.accumed.msls.payer.InsuranceCard;

import com.accumed.msls.payer.NASClient;
import com.accumed.msls.pools.nas.NASFactory;
import com.accumed.msls.pools.nas.NASPool;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.pool2.impl.AbandonedConfig;

/**
 * @author waltasseh
 */
public class NasClientThread implements Runnable {

    String encType;
    String eid;
    InsuranceCard card = null;
    String payerType;

    Account account;
    private static final ConcurrentHashMap<Account, NASPool> NAS_POOLS = new ConcurrentHashMap();

    public NasClientThread(Account account, String encType, String eid, String payerType) {
        this.encType = encType;
        this.eid = eid;
        this.account = account;
        this.payerType = payerType;
    }

    @Override
    public void run() {
        NASClient nas = null;
        NASPool poll = null;
        Logger.getLogger(NasClientThread.class.getName()).log(Level.INFO, "Start Nas lookup for={0}", eid);

        if ((poll = getPool()) == null) {
            card = new InsuranceCard("Invalid scrapping pool", -100);
            return;
        }
        try {
            //card = NasClient.getInstance().getCardByEID(encType, eid);
            nas = (NASClient) poll.borrowObject();
            card = nas.getCardByEId(eid, encType);
            poll.returnObject(nas);
            nas = null;
        } catch (Exception ex) {
            if (nas != null) {
                try {
                    poll.invalidateObject(nas);
                } catch (Exception ex1) {
                    Logger.getLogger(NasClientThread.class.getName()).log(Level.SEVERE, null, ex1);
                }
                poll.returnObject(nas);
            }

            Logger.getLogger(NasClientThread.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            if (card == null) {
                card = new InsuranceCard("Nas login error", -5);
            }
        }
        if (card != null && card.getSuccess() > 0) {
            card.setAuthNo("TPA004");
            card.setTpa("TPA004");
            card.setInsurers("TPA004 | NAS ADMINISTRATION SERVICES LIMITED");
            if (card.getStartDate() == null && card.getExpiryDate() != null & Utils.isContains2Dates(card.getExpiryDate())) {
                card.setStartDate(Utils.formatDate(Utils.parseStartDate(card.getExpiryDate()), "dd/MM/yyyy"));
                card.setExpiryDate(Utils.formatDate(Utils.parseExpiryDate(card.getExpiryDate()), "dd/MM/yyyy"));
            }
        }
    }

    public InsuranceCard getCard() {
        return card;
    }

    private NASPool getPool() {
        NASPool ret;
        if ((ret = NAS_POOLS.get(account)) != null) {
            return ret;
        } else if (account != null) {
            ret = new NASPool(new NASFactory(account));
            try {
                AbandonedConfig nasPool_abandonedConfig = new AbandonedConfig();
                nasPool_abandonedConfig.setRemoveAbandonedOnMaintenance(true);
                nasPool_abandonedConfig.setRemoveAbandonedTimeout(60 * 2); //2 minutes --if the object did not return within 2 minutes
                nasPool_abandonedConfig.setLogAbandoned(true);
                ret.setAbandonedConfig(nasPool_abandonedConfig);
                ret.setNumTestsPerEvictionRun(-1);
                ret.setMinEvictableIdleTimeMillis(1000 * 60 * 6); //5 minutes
                ret.setTimeBetweenEvictionRunsMillis(1000 * 60 * 3); //1.5 minutes
                ret.setTestWhileIdle(true);
                ret.setMinIdle(0);
                ret.preparePool();

                NAS_POOLS.put(account, ret);
                return ret;
            } catch (Exception ex) {
                Logger.getLogger(NasClientThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
