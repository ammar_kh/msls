package com.accumed.msls.callers;

import com.accumed.msls.model.Account;
import com.accumed.msls.model.Utils;
import com.accumed.msls.payer.InsuranceCard;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.accumed.msls.payer.NeuronClient;
import com.accumed.msls.pools.neuron.NEURONFactory;
import com.accumed.msls.pools.neuron.NEURONPool;
import org.apache.commons.pool2.impl.AbandonedConfig;

public class NeuronClientThread implements Runnable {
    String encType;
    String eid;
    InsuranceCard card = null;

    //private static final NEURONPool NEURON_POOL;
    Account account;
    private static final ConcurrentHashMap<Account, NEURONPool> NEURON_POOLS = new ConcurrentHashMap();

    public NeuronClientThread(Account account, String encType, String eid) {
        this.encType = encType;
        this.eid = eid;
        this.account = account;
    }

    @Override
    public void run() {
        NeuronClient neuron = null;
        NEURONPool poll;

        if ((poll = getPool()) == null) {
            card = new InsuranceCard("Invalid scrapping pool", -100);
            return;
        }

        Logger.getLogger(NeuronClientThread.class.getName()).log(Level.INFO, "Start Neuron lookup for={0}", eid);
        try {
            neuron = (NeuronClient) poll.borrowObject();
            card = neuron.getCardByEId(eid, encType);
            poll.returnObject(neuron);
            neuron = null;
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(NeuronClientThread.class.getName()).log(Level.SEVERE, null, ex);

            if (neuron != null) {
                try {
                    poll.invalidateObject(neuron);
                    neuron = null;
                } catch (Exception ex1) {
                    Logger.getLogger(NeuronClientThread.class.getName()).log(Level.SEVERE, null, ex1);
                }
                card = new InsuranceCard(ex);
            } else {
                card = new InsuranceCard("Neuron login error", -5);
            }
        }
        if (card != null && card.getSuccess() > 0) {
            card.setAuthNo("TPA001");
            card.setTpa("TPA001");
            card.setInsurers("TPA001 | Neuron LLC - DHA");
            if (card.getStartDate() == null && card.getExpiryDate() != null & Utils.isContains2Dates(card.getExpiryDate())) {
                card.setStartDate(Utils.formatDate(Utils.parseStartDate(card.getExpiryDate()), "dd/MM/yyyy"));
                card.setExpiryDate(Utils.formatDate(Utils.parseExpiryDate(card.getExpiryDate()), "dd/MM/yyyy"));
            }
        }
    }

    public InsuranceCard getCard() {
        return card;
    }

    private NEURONPool getPool() {
        NEURONPool ret = null;
        if ((ret = NEURON_POOLS.get(account)) != null) {
            return ret;
        } else if (account != null) {
            ret = new NEURONPool(new NEURONFactory(account));

            try {
                AbandonedConfig neuronPool_abandonedConfig = new AbandonedConfig();
                neuronPool_abandonedConfig.setRemoveAbandonedOnMaintenance(true);
                neuronPool_abandonedConfig.setRemoveAbandonedTimeout(60 * 6); //6 minutes --if the object did not return within 2 minutes
                neuronPool_abandonedConfig.setLogAbandoned(true);
                ret.setAbandonedConfig(neuronPool_abandonedConfig);
                ret.setNumTestsPerEvictionRun(-1);
                ret.setMinEvictableIdleTimeMillis(1000 * 60 * 6); //5 minutes
                ret.setTimeBetweenEvictionRunsMillis(1000 * 60 * 3); //1.5 minutes
                ret.setTestWhileIdle(true);
                ret.setMinIdle(0);
                ret.preparePool();

                NEURON_POOLS.put(account, ret);
                return ret;

            } catch (Exception ex) {
                Logger.getLogger(NeuronClientThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
