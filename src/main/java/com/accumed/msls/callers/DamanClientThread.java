/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.callers;

import com.accumed.msls.model.Account;
import com.accumed.msls.model.Utils;
import com.accumed.msls.payer.DAMANClient;
import com.accumed.msls.payer.InsuranceCard;
import com.accumed.msls.pools.daman.DAMANFactory;
import com.accumed.msls.pools.daman.DAMANPool;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.pool2.impl.AbandonedConfig;

/**
 * @author waltasseh
 */
public class DamanClientThread implements Runnable {

    String encType;
    String eid;
    InsuranceCard card = null;
    Account account;
    private static final ConcurrentHashMap<Account, DAMANPool> DAMAN_POOLS = new ConcurrentHashMap();

    public DamanClientThread(Account account, String encType, String eid) {
        this.encType = encType;
        this.eid = eid;
        this.account = account;
    }

    @Override
    public void run() {
        DAMANClient daman = null;
        DAMANPool poll;

        Logger.getLogger(DamanClientThread.class.getName()).log(Level.INFO, "Start Daman lookup for={0}", eid);

        if ((poll = getPool()) == null) {
            card = new InsuranceCard("Invalid scrapping pool", -100);
            return;
        }

        try {
            daman = (DAMANClient) poll.borrowObject();
            String dos = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
            card = daman.getCardByEId(eid, dos);
            poll.returnObject(daman);
            System.out.println(poll.getCount());
            daman = null;
        } catch (Exception ex) {
            if (daman != null) {
                try {
                    poll.invalidateObject(daman);
                } catch (Exception ex1) {
                    Logger.getLogger(DamanClientThread.class.getName()).log(Level.SEVERE, null, ex1);
                }
                poll.returnObject(daman);
            }
            Logger.getLogger(DamanClientThread.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            if (card == null) {
                card = new InsuranceCard("Daman login error", -5);
            }
        }
        if (card != null && card.getSuccess() > 0) {
            card.setAuthNo("INS026");
            card.setTpa("INS026");
            card.setInsurers("INS026 | NATIONAL HEALTH INSURANCE COMPANY (DAMAN) ");
            if (card.getStartDate() == null && card.getExpiryDate() != null & Utils.isContains2Dates(card.getExpiryDate())) {
                card.setStartDate(Utils.formatDate(Utils.parseStartDate(card.getExpiryDate()), "dd/MM/yyyy"));
                card.setExpiryDate(Utils.formatDate(Utils.parseExpiryDate(card.getExpiryDate()), "dd/MM/yyyy"));
            }
        }
    }

    public InsuranceCard getCard() {
        return card;
    }

    private DAMANPool getPool() {
        DAMANPool ret;
        if ((ret = DAMAN_POOLS.get(account)) != null) {
            return ret;
        } else if (account != null) {
            ret = new DAMANPool(new DAMANFactory(account));
            try {
                AbandonedConfig damanPool_abandonedConfig = new AbandonedConfig();
                damanPool_abandonedConfig.setRemoveAbandonedOnMaintenance(true);
                damanPool_abandonedConfig.setRemoveAbandonedTimeout(60 * 5); //5 minutes --if the object did not return within 2 minutes
                damanPool_abandonedConfig.setLogAbandoned(true);

                ret.setAbandonedConfig(damanPool_abandonedConfig);
                ret.setNumTestsPerEvictionRun(-1);
                ret.setMinEvictableIdleTimeMillis(1000 * 60 * 6); //6 minutes
                ret.setTimeBetweenEvictionRunsMillis(1000 * 60 * 3); //3 minutes
                ret.setTestWhileIdle(true);
                ret.setMinIdle(0);
                ret.preparePool();

                DAMAN_POOLS.put(account, ret);
                return ret;
            } catch (Exception ex) {
                Logger.getLogger(DamanClientThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }
}
