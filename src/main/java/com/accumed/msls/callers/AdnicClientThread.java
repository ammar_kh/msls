/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.callers;

import com.accumed.msls.model.Account;
import com.accumed.msls.exception.LoginException;
import com.accumed.msls.model.Utils;
import com.accumed.msls.payer.ADNICClient;
import com.accumed.msls.payer.InsuranceCard;
import com.accumed.msls.pools.adnic.ADNICFactory;
import com.accumed.msls.pools.adnic.ADNICPool;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.pool2.impl.AbandonedConfig;

/**
 *
 * @author bmahow
 */
public class AdnicClientThread implements Runnable {

    String encType;
    String eid;
    InsuranceCard card = null;
    Account account;

    private static final ConcurrentHashMap<Account, ADNICPool> ADNIC_POOLS = new ConcurrentHashMap();

    public AdnicClientThread(Account account, String encType, String eid) {
        this.encType = encType;
        this.eid = eid;
        this.account = account;
    }

    @Override
    public void run() {
        ADNICClient adnic = null;
        ADNICPool poll;
        Logger.getLogger(AdnicClientThread.class.getName()).log(Level.INFO, "Start adnic lookup for={0}", eid);
        if ((poll = getPool()) == null) {
            card = new InsuranceCard("Invalid scrapping pool", -100);
            return;
        }
        try {
            adnic = (ADNICClient) poll.borrowObject();
            card = adnic.getCardByEId(eid);
            poll.returnObject(adnic);
            adnic = null;
        } catch (LoginException ex) {
            ex.printStackTrace();
            card = new InsuranceCard("Adnic Login Failed", -5);
        } catch (Exception e) {
            if (adnic != null) {
                try {
                    poll.invalidateObject(adnic);
                } catch (Exception e1) {
                    Logger.getLogger(AdnicClientThread.class.getName()).log(Level.SEVERE, null, e1);
                }
                poll.returnObject(adnic);
            }
            e.printStackTrace();
            if (card == null) {
                card = new InsuranceCard("Adnic login error", -5);
            }
            Logger.getLogger(AdnicClientThread.class.getName()).log(Level.SEVERE, null, e);
        }

        if (card != null && card.getSuccess() > 0) {
            card.setAuthNo("INS017");
            card.setTpa("INS017");
            card.setInsurers("INS017 | ABU DHABI NATIONAL INSURANCE COMPANY");
            if (card.getStartDate() == null && card.getExpiryDate() != null & Utils.isContains2Dates(card.getExpiryDate())) {
                card.setStartDate(Utils.formatDate(Utils.parseStartDate(card.getExpiryDate()), "dd/MM/yyyy"));
                card.setExpiryDate(Utils.formatDate(Utils.parseExpiryDate(card.getExpiryDate()), "dd/MM/yyyy"));
            }
        }
       if (card != null && card.getMessage().equals("User session expired!")) {
            if (adnic != null) {
                try {
                    poll.invalidateObject(adnic);
                } catch (Exception e1) {
                    Logger.getLogger(AdnicClientThread.class.getName()).log(Level.SEVERE, null, e1);
                }
                poll.returnObject(adnic);
            }
        }
    }

    public InsuranceCard getCard() {
        return card;
    }

    private ADNICPool getPool() {
        ADNICPool ret;
        if ((ret = ADNIC_POOLS.get(account)) != null) {
            return ret;
        } else if (account != null) {
            ret = new ADNICPool(new ADNICFactory(account));
            try {
                AbandonedConfig adnicPool_abandonedConfig = new AbandonedConfig();
                adnicPool_abandonedConfig.setRemoveAbandonedOnMaintenance(true);
                adnicPool_abandonedConfig.setRemoveAbandonedTimeout(15);
                //10 seconds --if the object did not return within 10 seconds
                adnicPool_abandonedConfig.setLogAbandoned(true);
                ret.setAbandonedConfig(adnicPool_abandonedConfig);
                ret.setNumTestsPerEvictionRun(-1);
//            ADNIC_POOL.setMinEvictableIdleTimeMillis(1000 * 60 * 6); //5 minutes
//            ADNIC_POOL.setTimeBetweenEvictionRunsMillis(1000 * 60 * 3); //1.5 minutes
//            ADNIC_POOL.setTestWhileIdle(true);
                ret.setMaxTotal(1);
                ret.setMaxWaitMillis(30 * 1000); //30 seconds
                ret.preparePool();

                ADNIC_POOLS.put(account, ret);
                return ret;
            } catch (Exception ex) {
                Logger.getLogger(AdnicClientThread.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return null;
    }

}
