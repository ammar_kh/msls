/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.callers;

import com.accumed.msls.model.Account;
import com.accumed.msls.exception.LoginException;
import com.accumed.msls.model.Utils;
import com.accumed.msls.payer.InsuranceCard;
import com.accumed.msls.payer.WhealthClient;
import com.accumed.msls.pools.whealth.WHEALTHFactory;
import com.accumed.msls.pools.whealth.WHEALTHPool;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.pool2.impl.AbandonedConfig;

/**
 *
 * @author bmahow
 */
public class WhealthClientThread implements Runnable {

    String encType;
    String eid;
    InsuranceCard card = null;
    Account account;

    private static final ConcurrentHashMap<Account, WHEALTHPool> WHEALTH_POOLS = new ConcurrentHashMap();

    public WhealthClientThread(Account account,String encType, String eid) {
        this.account = account;
        this.encType = encType;
        this.eid = eid;
    }
    
    
      @Override
    public void run() {
          WhealthClient whealth = null;
        WHEALTHPool poll = null;
        Logger.getLogger(WhealthClientThread.class.getName()).log(Level.INFO, "Start Whealth lookup for={0}", eid);

        if ((poll = getPool()) == null) {
            card = new InsuranceCard("Invalid scrapping pool", -100);
            return;
        }
        try {
            //card = NasClient.getInstance().getCardByEID(encType, eid);
            whealth = (WhealthClient) poll.borrowObject();
            card = whealth.getCardByEId(eid);
            poll.returnObject(whealth);
            whealth = null;
        }catch (LoginException ex) {
            ex.printStackTrace();
            card = new InsuranceCard("Whealth Login Failed", -5);
        } catch (Exception ex) {
            if (whealth != null) {
                try {
                    poll.invalidateObject(whealth);
                } catch (Exception ex1) {
                    Logger.getLogger(WhealthClientThread.class.getName()).log(Level.SEVERE, null, ex1);
                }
                poll.returnObject(whealth);
            }

            Logger.getLogger(WhealthClientThread.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            if (card == null) {
                card = new InsuranceCard("Whealth login error", -5);
            }
        }
        if (card != null && card.getSuccess() > 0) {
            card.setAuthNo("TPA032");
            card.setTpa("TPA032");
            card.setInsurers("TPA032 | WHEALTH INTERNATIONAL LLC");
            if (card.getStartDate() == null && card.getExpiryDate() != null & Utils.isContains2Dates(card.getExpiryDate())) {
                card.setStartDate(Utils.formatDate(Utils.parseStartDate(card.getExpiryDate()), "dd/MM/yyyy"));
                card.setExpiryDate(Utils.formatDate(Utils.parseExpiryDate(card.getExpiryDate()), "dd/MM/yyyy"));
            }
        }
    }

    public InsuranceCard getCard() {
        return card;
    }

    private WHEALTHPool getPool() {
        WHEALTHPool ret;
        if ((ret = WHEALTH_POOLS.get(account)) != null) {
            return ret;
        } else if (account != null) {
            ret = new WHEALTHPool(new WHEALTHFactory(account));
            try {
                AbandonedConfig whealthPool_abandonedConfig = new AbandonedConfig();
                whealthPool_abandonedConfig.setRemoveAbandonedOnMaintenance(true);
                whealthPool_abandonedConfig.setRemoveAbandonedTimeout(60 * 6); //4 minutes --if the object did not return within 2 minutes
                whealthPool_abandonedConfig.setLogAbandoned(true);
                ret.setAbandonedConfig(whealthPool_abandonedConfig);
                ret.setNumTestsPerEvictionRun(-1);
                ret.setMinEvictableIdleTimeMillis(1000 * 60 * 6); //6 minutes
                ret.setTimeBetweenEvictionRunsMillis(1000 * 60 * 3); //3 minutes
                ret.setTestWhileIdle(true);
                ret.setMinIdle(0);
                ret.preparePool();

                WHEALTH_POOLS.put(account, ret);
                return ret;
            } catch (Exception ex) {
                Logger.getLogger(WhealthClientThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

}
