/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.callers;

import com.accumed.msls.model.Account;
import com.accumed.msls.exception.LoginException;
import com.accumed.msls.model.Utils;
import com.accumed.msls.payer.AlBuhairaClient;
import com.accumed.msls.payer.InsuranceCard;
import com.accumed.msls.pools.albuhaira.ALBUHAIRAFactory;
import com.accumed.msls.pools.albuhaira.ALBUHAIRAPool;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.pool2.impl.AbandonedConfig;

/**
 *
 * @author bmahow
 */
public class AlbuhairaClientThread implements Runnable {

    String encType;
    String eid;
    InsuranceCard card = null;
    Account account;
    private static final ConcurrentHashMap<Account, ALBUHAIRAPool> ALBUHAIRA_POOLS = new ConcurrentHashMap();

    public AlbuhairaClientThread(Account account, String encType, String eid) {
        this.account = account;
        this.encType = encType;
        this.eid = eid;
    }

    @Override
    public void run() {
        AlBuhairaClient alBuhaira = null;
        ALBUHAIRAPool poll = null;
        Logger.getLogger(AlbuhairaClientThread.class.getName()).log(Level.INFO, "Start alBuhaira lookup for={0}", eid);
        if ((poll = getPool()) == null) {
            card = new InsuranceCard("Invalid scrapping pool", -100);
            return;
        }
        try {
            //card = NasClient.getInstance().getCardByEID(encType, eid);
            alBuhaira = (AlBuhairaClient) poll.borrowObject();
            card = alBuhaira.getCardByEId(eid);
            poll.returnObject(alBuhaira);
            alBuhaira = null;
        } catch (LoginException ex) {
            ex.printStackTrace();
            card = new InsuranceCard("alBuhaira Login Failed", -5);
        } catch (Exception ex) {
            if (alBuhaira != null) {
                try {
                    poll.invalidateObject(alBuhaira);
                } catch (Exception ex1) {
                    Logger.getLogger(AlbuhairaClientThread.class.getName()).log(Level.SEVERE, null, ex1);
                }
                poll.returnObject(alBuhaira);
            }

            Logger.getLogger(AlbuhairaClientThread.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            if (card == null) {
                card = new InsuranceCard("AlBuhaira login error", -5);
            }
        }
        if (card != null && card.getSuccess() > 0) {
            card.setAuthNo("INS020");
            card.setTpa("INS020");
            card.setInsurers("INS020 | AL-BUHAIRA NATIONAL INSURANCE COMPANY");
           if (card.getStartDate() == null && card.getExpiryDate() != null & Utils.isContains2Dates(card.getExpiryDate())) {
                card.setStartDate(Utils.formatDate(Utils.parseStartDate(card.getExpiryDate()), "dd/MM/yyyy"));
                card.setExpiryDate(Utils.formatDate(Utils.parseExpiryDate(card.getExpiryDate()), "dd/MM/yyyy"));
            }
        }
    }

    public InsuranceCard getCard() {
        return card;
    }

    private ALBUHAIRAPool getPool() {
        ALBUHAIRAPool ret;
        if ((ret = ALBUHAIRA_POOLS.get(account)) != null) {
            return ret;
        } else if (account != null) {
            ret = new ALBUHAIRAPool(new ALBUHAIRAFactory(account));
            try {
                AbandonedConfig albuhairaPool_abandonedConfig = new AbandonedConfig();
                albuhairaPool_abandonedConfig.setRemoveAbandonedOnMaintenance(true);
                albuhairaPool_abandonedConfig.setRemoveAbandonedTimeout(60 * 5); //2 minutes --if the object did not return within 2 minutes
                albuhairaPool_abandonedConfig.setLogAbandoned(true);
                ret.setAbandonedConfig(albuhairaPool_abandonedConfig);
                ret.setNumTestsPerEvictionRun(-1);
                ret.setMinEvictableIdleTimeMillis(1000 * 60 * 6); //5 minutes
                ret.setTimeBetweenEvictionRunsMillis(1000 * 60 * 3); //1.5 minutes
                ret.setTestWhileIdle(true);
                ret.setMinIdle(0);
                ret.preparePool();
                ALBUHAIRA_POOLS.put(account, ret);
                return ret;
            } catch (Exception ex) {
                Logger.getLogger(AlbuhairaClientThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

}
