/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.callers;

import com.accumed.msls.model.Account;
import com.accumed.msls.exception.LoginException;
import com.accumed.msls.model.Utils;
import com.accumed.msls.payer.AafiyaClient;
import com.accumed.msls.payer.InsuranceCard;
import com.accumed.msls.pools.aafiya.AAFIYAFactory;
import com.accumed.msls.pools.aafiya.AAFIYAPool;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.pool2.impl.AbandonedConfig;

/**
 * @author bmahow
 */
public class AafiyaClientThread implements Runnable {
    String encType;
    String eid;
    InsuranceCard card = null;

    Account account;
    private static final ConcurrentHashMap<Account, AAFIYAPool> AAFIYA_POOLS = new ConcurrentHashMap();

    public AafiyaClientThread(Account account, String encType, String eid) {
        this.account = account;
        this.encType = encType;
        this.eid = eid;
    }

    @Override
    public void run() {
        AafiyaClient aafiya = null;
        AAFIYAPool poll;
        Logger.getLogger(AafiyaClientThread.class.getName()).log(Level.INFO, "Start Aafiya lookup for={0}", eid);

        if ((poll = getPool()) == null) {
            card = new InsuranceCard("Invalid scrapping pool", -100);
            return;
        }
        try {
            aafiya = (AafiyaClient) poll.borrowObject();
            card = aafiya.getCardByEID(eid);
            poll.returnObject(aafiya);
            aafiya = null;
        } catch (LoginException ex) {
            ex.printStackTrace();
            card = new InsuranceCard("Aafiya Login Failed", -5);
        } catch (Exception e) {
            if (aafiya != null) {
                try {
                    poll.invalidateObject(aafiya);
                } catch (Exception e1) {
                    Logger.getLogger(AafiyaClientThread.class.getName()).log(Level.SEVERE, null, e1);
                }
                poll.returnObject(aafiya);
            }
            e.printStackTrace();
            if (card == null) {
                card = new InsuranceCard("Aafiya login error", -5);
            }
            Logger.getLogger(AafiyaClientThread.class.getName()).log(Level.SEVERE, null, e);
        }

        if (card != null && card.getSuccess() > 0) {
            card.setAuthNo("TPA026");
            card.setTpa("TPA026");
            card.setInsurers("TPA026 | AAFIYA MEDICAL BILLING SERVICES L.L.C");
            if (card.getStartDate() == null && card.getExpiryDate() != null & Utils.isContains2Dates(card.getExpiryDate())) {
                card.setStartDate(Utils.formatDate(Utils.parseStartDate(card.getExpiryDate()), "dd/MM/yyyy"));
                card.setExpiryDate(Utils.formatDate(Utils.parseExpiryDate(card.getExpiryDate()), "dd/MM/yyyy"));
            }
        }
    }

    public InsuranceCard getCard() {
        return card;
    }

    private AAFIYAPool getPool() {
        AAFIYAPool ret;
        if ((ret = AAFIYA_POOLS.get(account)) != null) {
            return ret;
        } else if (account != null) {
            ret = new AAFIYAPool(new AAFIYAFactory(account));
            try {
                AbandonedConfig aafiyaPool_abandonedConfig = new AbandonedConfig();
                aafiyaPool_abandonedConfig.setRemoveAbandonedOnMaintenance(true);
                aafiyaPool_abandonedConfig.setRemoveAbandonedTimeout(60 * 10); //6 minutes --if the object did not return within 2 minutes
                aafiyaPool_abandonedConfig.setLogAbandoned(true);
                ret.setAbandonedConfig(aafiyaPool_abandonedConfig);
                ret.setNumTestsPerEvictionRun(-1);
                ret.setMinEvictableIdleTimeMillis(1000 * 60 * 6); //6 minutes
                ret.setTimeBetweenEvictionRunsMillis(1000 * 60 * 3); //3 minutes
                ret.setTestWhileIdle(true);
                ret.setMinIdle(0);
                ret.preparePool();

                AAFIYA_POOLS.put(account, ret);
                return ret;
            } catch (Exception ex) {
                Logger.getLogger(AafiyaClientThread.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return null;
    }

}
