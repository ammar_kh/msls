/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.callers;

import com.accumed.msls.model.Account;
import com.accumed.msls.exception.LoginException;
import com.accumed.msls.model.Utils;
import com.accumed.msls.payer.InsuranceCard;
import com.accumed.msls.payer.NextCareClient;
import com.accumed.msls.pools.nextcare.NEXTCAREFactory;
import com.accumed.msls.pools.nextcare.NEXTCAREPool;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.pool2.impl.AbandonedConfig;

/**
 *
 * @author waltasseh
 */
public class NextcareClientThread implements Runnable {

    String encType;
    String eid;
    InsuranceCard card = null;

    Account account;
    private static final ConcurrentHashMap<Account, NEXTCAREPool> NEXTCARE_POOLS = new ConcurrentHashMap();

    public NextcareClientThread(Account account, String encType, String eid) {
        this.account = account;
        this.encType = encType;
        this.eid = eid;
    }

    @Override
    public void run() {
        NextCareClient nextcare = null;
        NEXTCAREPool poll = null;
        Logger.getLogger(NextcareClientThread.class.getName()).log(Level.INFO, "Start Nextcare lookup for={0}", eid);
        if ((poll = getPool()) == null) {
            card = new InsuranceCard("Invalid scrapping pool", -100);
            return;
        }
        try {
            //card = NextcareClient.getInstance().getCardByEID(encType, eid.replace("-", ""));
            nextcare = (NextCareClient) poll.borrowObject();
            card = nextcare.getCardByEId( eid.replace("-", ""),encType);
            poll.returnObject(nextcare);
            nextcare = null;
        } catch (LoginException ex) {
            ex.printStackTrace();
            card = new InsuranceCard("Nextcare Login Failed", -5);
        } catch (Exception ex) {
            if (nextcare != null) {
                try {
                    poll.invalidateObject(nextcare);
                } catch (Exception ex1) {
                    Logger.getLogger(NextcareClientThread.class.getName()).log(Level.SEVERE, null, ex1);
                }
                poll.returnObject(nextcare);
            }
            ex.printStackTrace();
            if (card == null) {
                card = new InsuranceCard("Nextcare login error", -5);
            }
            Logger.getLogger(NextcareClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (card != null && card.getSuccess() > 0) {
            card.setAuthNo("TPA002");
            card.setTpa("TPA002");
            card.setInsurers("TPA002 | ARAB GULF HEALTH SERVICES (NEXTCARE) ");
            if (card.getStartDate() == null && card.getExpiryDate() != null & Utils.isContains2Dates(card.getExpiryDate())) {
                card.setStartDate(Utils.formatDate(Utils.parseStartDate(card.getExpiryDate()), "dd/MM/yyyy"));
                card.setExpiryDate(Utils.formatDate(Utils.parseExpiryDate(card.getExpiryDate()), "dd/MM/yyyy"));
            }
        }
    }

    public InsuranceCard getCard() {
        return card;
    }

    private NEXTCAREPool getPool() {
        NEXTCAREPool ret;
        if ((ret = NEXTCARE_POOLS.get(account)) != null) {
            return ret;
        } else if (account != null) {
            ret = new NEXTCAREPool(new NEXTCAREFactory(account));
            try {
                AbandonedConfig nextCarePool_abandonedConfig = new AbandonedConfig();
                nextCarePool_abandonedConfig.setRemoveAbandonedOnMaintenance(true);
                nextCarePool_abandonedConfig.setRemoveAbandonedTimeout(60 * 2); //2 minutes --if the object did not return within 2 minutes
                nextCarePool_abandonedConfig.setLogAbandoned(true);
                ret.setAbandonedConfig(nextCarePool_abandonedConfig);
                ret.setNumTestsPerEvictionRun(-1);
                ret.setMinEvictableIdleTimeMillis(1000 * 60 * 6); //5 minutes
                ret.setTimeBetweenEvictionRunsMillis(1000 * 60 * 3); //1.5 minutes
                ret.setTestWhileIdle(true);
                ret.setMinIdle(0);
                ret.preparePool();

                NEXTCARE_POOLS.put(account, ret);
                return ret;

            } catch (Exception ex) {
                Logger.getLogger(NextcareClientThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

}
