/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.callers;

import com.accumed.msls.model.Account;
import com.accumed.msls.model.Utils;
import com.accumed.msls.payer.InsuranceCard;
import com.accumed.msls.payer.THIQAClient;
import com.accumed.msls.pools.thiqa.THIQAFactory;
import com.accumed.msls.pools.thiqa.THIQAPool;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.pool2.impl.AbandonedConfig;

/**
 *
 * @author waltasseh
 */
public class ThiqaClientThread implements Runnable {

    String encType;
    String eid;
    InsuranceCard card = null;

    //private static final THIQAPool THIQA_POOL;
    Account account;
    private static final ConcurrentHashMap<Account, THIQAPool> THIQA_POOLS = new ConcurrentHashMap();

    public ThiqaClientThread(Account account, String encType, String eid) {
        this.encType = encType;
        this.eid = eid;
        this.account = account;
    }

    @Override
    public void run() {
        THIQAClient thiqa = null;
        THIQAPool poll;

        Logger.getLogger(ThiqaClientThread.class.getName()).log(Level.INFO, "Start Thiqa lookup for={0}", eid);

        if ((poll = getPool()) == null) {
            card = new InsuranceCard("Invalid scrapping pool", -100);
            return;
        }

        try {
            thiqa = (THIQAClient) poll.borrowObject();
          //  String[] eidParts = eid.split("-");
            card = thiqa.getCardByEId(eid);
            try {
                poll.returnObject(thiqa);
            } catch (java.lang.IllegalStateException e) {
                poll.invalidateObject(thiqa);
                thiqa = null;
            }
            thiqa = null;
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(ThiqaClientThread.class.getName()).log(Level.SEVERE, null, ex);

            if (thiqa != null) {
                try {
                    poll.invalidateObject(thiqa);
                    thiqa = null;
                } catch (Exception ex1) {
                    Logger.getLogger(ThiqaClientThread.class.getName()).log(Level.SEVERE, null, ex1);
                }
                card = new InsuranceCard(ex);
            } else {
                card = new InsuranceCard("Thiqa login error", -5);
            }
        }

        if (card != null && card.getSuccess() > 0) {
            card.setAuthNo("INS078");
            card.setTpa("INS078");
            card.setInsurers("INS078 | ABU DHABI MINISTRY OF FINANCE ");
            if (card.getStartDate() == null && card.getExpiryDate() != null & Utils.isContains2Dates(card.getExpiryDate())) {
                card.setStartDate(Utils.formatDate(Utils.parseStartDate(card.getExpiryDate()), "dd/MM/yyyy"));
                card.setExpiryDate(Utils.formatDate(Utils.parseExpiryDate(card.getExpiryDate()), "dd/MM/yyyy"));
            }
        }

    }

    public InsuranceCard getCard() {
        return card;
    }

    private THIQAPool getPool() {
        THIQAPool ret;
        if ((ret = THIQA_POOLS.get(account)) != null) {
            return ret;
        } else if (account != null) {
            ret = new THIQAPool(new THIQAFactory(account));

            try {
                AbandonedConfig thiqaPool_abandonedConfig = new AbandonedConfig();
                thiqaPool_abandonedConfig.setRemoveAbandonedOnMaintenance(true);
                thiqaPool_abandonedConfig.setRemoveAbandonedTimeout(60 * 2); //2 minutes --if the object did not return within 2 minutes
                thiqaPool_abandonedConfig.setLogAbandoned(true);
                ret.setAbandonedConfig(thiqaPool_abandonedConfig);
                ret.setNumTestsPerEvictionRun(-1);
                ret.setMinEvictableIdleTimeMillis(1000 * 60 * 6); //5 minutes
                ret.setTimeBetweenEvictionRunsMillis(1000 * 60 * 3); //1.5 minutes
                ret.setTestWhileIdle(true);
                ret.setMinIdle(0);
                ret.preparePool();
                THIQA_POOLS.put(account, ret);
                return ret;
            } catch (Exception ex) {
                Logger.getLogger(ThiqaClientThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
