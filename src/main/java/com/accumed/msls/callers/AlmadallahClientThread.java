/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.callers;

import com.accumed.msls.model.Account;
import com.accumed.msls.model.Utils;
import com.accumed.msls.payer.ALMADALLAHClient;
import com.accumed.msls.payer.InsuranceCard;
import com.accumed.msls.pools.almadallah.ALMADALLAHFactory;
import com.accumed.msls.pools.almadallah.ALMADALLAHPool;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.pool2.impl.AbandonedConfig;

/**
 *
 * @author waltasseh
 */
public class AlmadallahClientThread implements Runnable {

    String encType;
    String eid;
    InsuranceCard card = null;
    Account account;
    private static final ConcurrentHashMap<Account, ALMADALLAHPool> ALMADALLAH_POOLS = new ConcurrentHashMap();

    public AlmadallahClientThread(Account account, String encType, String eid) {
        this.encType = encType;
        this.eid = eid;
        this.account = account;
    }

    @Override
    public void run() {
        ALMADALLAHClient almadallah = null;
        ALMADALLAHPool poll = null;
        Logger.getLogger(AlmadallahClientThread.class.getName()).log(Level.INFO, "Start Almadallah lookup for={0}", eid);

        if ((poll = getPool()) == null) {
            card = new InsuranceCard("Invalid scrapping pool", -100);
            return;
        }

        try {
            //card = AlmadallahClient.getInstance().getCardByEID(encType, eid);
            almadallah = (ALMADALLAHClient) poll.borrowObject();
            card = almadallah.getCardByEId( eid, encType);
            poll.returnObject(almadallah);
            almadallah = null;
        } catch (Exception ex) {
            if (almadallah != null) {
                try {
                    poll.invalidateObject(almadallah);
                } catch (Exception ex1) {
                    Logger.getLogger(AlmadallahClientThread.class.getName()).log(Level.SEVERE, null, ex1);
                }
                poll.returnObject(almadallah);
            }
            Logger.getLogger(AlmadallahClientThread.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            if (card == null) {
                card = new InsuranceCard("Almadallah login error", -5);
            }
        }

        if (card != null && card.getSuccess() > 0) {
            card.setAuthNo("TPA003");
            card.setTpa("TPA003");
            card.setInsurers("TPA003 | Almadallah Healthcare Management FZ-LLC  - DHA");
            if (card.getStartDate() == null && card.getExpiryDate() != null & Utils.isContains2Dates(card.getExpiryDate())) {
                card.setStartDate(Utils.formatDate(Utils.parseStartDate(card.getExpiryDate()), "dd/MM/yyyy"));
                card.setExpiryDate(Utils.formatDate(Utils.parseExpiryDate(card.getExpiryDate()), "dd/MM/yyyy"));
            }
        }
    }

    public InsuranceCard getCard() {
        return card;
    }

    private ALMADALLAHPool getPool() {
        ALMADALLAHPool ret = null;
        if ((ret = ALMADALLAH_POOLS.get(account)) != null) {
            return ret;
        } else if (account != null) {
            ret = new ALMADALLAHPool(new ALMADALLAHFactory(account));
            try {
                //ALMADALLAH_POOL
                AbandonedConfig almadallahPool_abandonedConfig = new AbandonedConfig();
                almadallahPool_abandonedConfig.setRemoveAbandonedOnMaintenance(true);
                almadallahPool_abandonedConfig.setRemoveAbandonedTimeout(60 * 2); //2 minutes --if the object did not return within 2 minutes
                almadallahPool_abandonedConfig.setLogAbandoned(true);
                ret.setAbandonedConfig(almadallahPool_abandonedConfig);
                ret.setNumTestsPerEvictionRun(-1);
                ret.setMinEvictableIdleTimeMillis(1000 * 60 * 6); //5 minutes
                ret.setTimeBetweenEvictionRunsMillis(1000 * 60 * 3); //1.5 minutes
                ret.setTestWhileIdle(true);
                ret.setMinIdle(0);
                ret.preparePool();
                ALMADALLAH_POOLS.put(account, ret);
                return ret;

            } catch (Exception ex) {
                Logger.getLogger(AlmadallahClientThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
