package com.accumed.msls.service;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author AKhalifa
 * class that will use as service for define function will use in web form
 */
public interface WebService {

    /*
     * login function will use for login to login form by use web driver
     * WebDriver: web driver selenium object
     * url: url for web form will call by web driver for login form
     * userId:  html element id that will handle user name text
     * userValue: value of user name will login to system
     * pwd: html element id will handle password text
     * pwdValue: value of password of user will login
     * submit button: expression of css that handle submit button
     * timeOfRequest: time will send to thread for sleep until request response */
    void login(WebDriver webDriver, String url, String userId, String userValue, String pwd, String pwdValue, int typeOfSubmit, String submitButton, int timeOfRequest) throws InterruptedException;

    /**
     * @param webDriver using for path current  webDriver
     */
    boolean checkIsPageFound(WebDriver webDriver, int typeOfContains, String containsOrId);

    void changeFrame(WebDriver driver, String frameXpath, String nameOfIframe) throws InterruptedException;

    boolean isPopUp(WebDriver driver);

    WebElement moveToHideElement(WebDriver driver, int typeOfElement, String elementName);
}
