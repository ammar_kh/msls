package com.accumed.msls.service;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.Iterator;
import java.util.Set;


/**
 * @Author by [AKhalifa]
 * service implementation on webservice
 */

public class WebServiceImp implements WebService {


    @Override
    public void login(WebDriver webDriver, String url, String userId, String userValue, String pwd, String pwdValue, int typeOfSubmit, String submitButton, int timeOfRequest) throws InterruptedException {
        if (webDriver != null) {
            if (!url.isEmpty()) {
                webDriver.navigate().to(url);
                //disable css .
                JavascriptExecutor js = (JavascriptExecutor) webDriver;
                js.executeScript("document.head.parentNode.removeChild(document.head);");
            }

            Thread.sleep(1000);

            webDriver.findElement(By.id(userId)).sendKeys(userValue);
            webDriver.findElement(By.id(pwd)).sendKeys(pwdValue);
            //check from type of submit button 1- id, 2- cssSelector , 3- className
            switch (typeOfSubmit) {
                case 1:
                    webDriver.findElement(By.id(submitButton)).click();
                    break;
                case 2:

                    webDriver.findElement(By.cssSelector(submitButton)).click();
                    break;
                case 3:
                    webDriver.findElement(By.xpath(submitButton)).click();
                    break;
                case 4:
                    webDriver.findElement(By.className(submitButton)).click();
                    break;
            }

            Thread.sleep(timeOfRequest);
        }


    }


    @Override
    public boolean checkIsPageFound(WebDriver webDriver, int typeOfContains, String containsOrId) {
        String windows = webDriver.getWindowHandle();
        System.out.println(windows.trim());
        //check from type of contains
        if (windows.contains(containsOrId)) {
            return true;
        }

        return false;
    }

    @Override
    public void changeFrame(WebDriver driver, String frameXpath, String nameOfIframe) throws InterruptedException {
        new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(frameXpath)));

        driver.switchTo().defaultContent();
        driver.switchTo().frame(nameOfIframe);
        Thread.sleep(800);
    }

    @Override
    public boolean isPopUp(WebDriver driver) {
        String mainHandle = driver.getWindowHandle();
        // to handle all new opened windows
        Set<String> windowHandles = driver.getWindowHandles();

        Iterator<String> it = windowHandles.iterator();
        while (it.hasNext()) {
            String childWindow = it.next();
            if (!mainHandle.equalsIgnoreCase(childWindow)) {
                driver.switchTo().window(childWindow);
                return true;
            }
        }
        return false;
    }

    @Override
    public WebElement moveToHideElement(WebDriver driver, int typeOfElement, String elementName) {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(By.id("ctl00_ContentPlaceHolderBody_cmbType_chosen"))).perform();
        driver.findElement(By.id("ctl00_ContentPlaceHolderBody_cmbType_chosen")).click();
        return null;
    }
}
