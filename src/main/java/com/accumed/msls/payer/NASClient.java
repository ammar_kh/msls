package com.accumed.msls.payer;


import com.accumed.msls.model.Account;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author AKhalifa
 * class for implements login, fetch insaurance card information for nas client.
 */
public class NASClient extends Client {

    private static final String LOGIN_URL = "https://jet.ntouch.cloud/jet/?AspxAutoDetectCookieSupport=1";
    private static final String MAIN_URL = "https://jet.ntouch.cloud/jet/Landing.aspx";
    private static final String CARD_URL = "https://www.almadallah.ae/provider/MemberInfo.aspx";

    public NASClient() {
        this(null);
    }

    public NASClient(Account account) {
        super(account);
    }

    public static NASClient getInstance(Account account) throws Exception {
        NASClient instance = new NASClient(account);
        instance.login();
        return instance;
    }

    /**
     * @author AKhalifa
     * login function for Nas client
     */
    @Override
    public boolean login() {
        long mi = new Date().getTime();
        Logger.getLogger(NASClient.class.getName()).log(Level.INFO, new Date() + "{0}...** Start login Nas Client ...");

        try {
            this.driver.navigate().to(LOGIN_URL);
            disableCss();
            WebDriverWait wait = new WebDriverWait(this.driver, 10);
            wait.until(ExpectedConditions.visibilityOf(this.driver.findElement(By.id("txtEmail"))));

            this.webService.login(this.driver, "", "txtEmail", account.getUser(), "txtPassword", account.getPass(), 1, "btnSignIn", 4000);
            // disableCss();
            if (this.driver.findElements(By.id("aEligibility")).size() != 0) {
                this.driver.findElement(By.id("aEligibility")).click();
            }

            if (this.driver.findElements(By.id("jconfirm-box36885")).size() != 0) {
                Logger.getLogger(NASClient.class.getName()).log(Level.INFO, this.driver.findElement(By.id("jconfirm-box36885")).getText());
            }
//            this.driver.findElement(By.id("UsersModalAnnoucement"));
//            this.driver.findElement(By.id("ucShowAnnoucement")).click();

            Logger.getLogger(NASClient.class.getName()).log(Level.INFO, new Date() + " ,...** Finish login Nas Client ... Time(s):" + ((new Date().getTime() - mi) / 1000));

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(NASClient.class.getName()).log(Level.INFO, new Date() + "Nas unknown login error");
            this.driver.quit();
        }
        return false;
    }

    /**
     * @return InsuranceCard object
     * @author AKhalifa
     */
    @Override
    public InsuranceCard getCardByEId(String eId) {
        InsuranceCard card = null;
        Logger.getLogger(NASClient.class.getName()).log(Level.INFO, new Date() + " *start fetching Nas Insurance card information");
        Long mi = new Date().getTime();


        try {
            if (this.driver.getCurrentUrl().endsWith("/Landing.aspx")) {
                //this.driver.navigate().refresh();
                Thread.sleep(2000);
                if (this.driver.findElements(By.xpath("//*[@id=\"dvEligibilityCheckUsing\"]/div/div[1]/label")).size() != 0) {
                    this.driver.findElement(By.xpath("//*[@id=\"dvEligibilityCheckUsing\"]/div/div[1]/label")).click();
                }

                this.driver.findElement(By.id("EligbilityAddNationalID")).sendKeys(eId);
                this.driver.findElement(By.id("ddlTreatmentbasis_chosen")).click();
                this.driver.findElement(By.xpath("//*[@id=\"ddlTreatmentbasis_chosen\"]/div/ul/li[3]")).click();
                this.driver.findElement(By.id("txtAddBenefPhone")).sendKeys("050 123 4567");
                this.driver.findElement(By.id("btnSubmitNewElgibility")).click();
//            Actions action = new Actions(this.driver);
//            action.moveToElement(this.driver.findElement(By.id("ctl00_ContentPlaceHolderBody_cmbType_chosen"))).perform();
//            this.driver.findElement(By.id("ctl00_ContentPlaceHolderBody_cmbType_chosen")).click();

                if (this.driver.findElements(By.id("jconfirm-box76667")).size() != 0) {
                    if (this.driver.findElement(By.id("jconfirm-box76667")).equals("Request member cardnumber is invalid")) {
                        card = new InsuranceCard("Request member card number is invalid", -1);
                    }
                }

                Thread.sleep(4000);
                if (this.driver.getCurrentUrl().endsWith("/EligibilityDetails.aspx")) {
                    if (this.driver.findElement(By.id("cphBody_rptResponseFile_dvResult_0")).getText().equals("Not Eligible")) {
                        card = new InsuranceCard("Not Eligible", -1);
                    } else if (this.driver.findElement(By.id("cphBody_rptResponseFile_dvResult_0")).getText().equals("Eligible")) {
                        card = new InsuranceCard();

                        String name = this.driver.findElement(By.xpath("//*[@id=\"cphBody_rptResponseFile_dvMemDet_0\"]/div[2]/div[1]/div[1]")).getText();
                        card.setName(name.substring(0, name.length() - 1));
                        card.setStartDate(this.driver.findElement(By.xpath("//*[@id=\"cphBody_rptResponseFile_dvMemDet_0\"]/div[2]/div[2]/div[1]")).getText());
                        String endDate = this.driver.findElement(By.xpath("//*[@id=\"cphBody_rptResponseFile_dvMemDet_0\"]/div[2]/div[2]/div[2]")).getText();
                        card.setExpiryDate(endDate.substring(2, endDate.length()));
                        String copay = this.driver.findElement(By.id("cphBody_rptResponseFile_dvCoveragesNotes_0")).getText();
                        copay += "\n" + this.driver.findElement(By.id("cphBody_rptResponseFile_dvMessages_0")).getText();
                        card.setCopay(copay);
                        this.driver.findElement(By.id("cphBody_rptResponseFile_aEligibilityMemberDetails_0")).click();
                        Thread.sleep(500);
                        if (this.driver.findElements(By.id("cphBody_upMemperDetails")).size() != 0) {
                            card.setTpa(this.driver.findElement(By.id("cphBody_rptMemberDetails_dvMemberValue_0")).getText());
                            card.setDateOfBirth(this.driver.findElement(By.id("cphBody_rptMemberDetails_dvMemberValue_3")).getText());
                            card.setGender(this.driver.findElement(By.id("cphBody_rptMemberDetails_dvMemberValue_4")).getText());
                            this.driver.findElement(By.xpath("//*[@id=\"mpeEligibilityDetailsModal\"]/div/div/div[1]/button")).click();
                        }
                        card.setSuccess(1);
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(NASClient.class.getName()).log(Level.INFO, "unknow nas client error : {0}", new Date());
        }
        this.driver.findElement(By.id("back")).click();
        return card;
    }

    @Override
    public InsuranceCard getCardByEId(String eId, String encType) {
        return getCardByEId(eId);
    }

    @Override
    public boolean isValid() throws InterruptedException {
        return false;
    }

    @Override
    public void destroyDrive() {
        this.driver.quit();
    }
}
