package com.accumed.msls.payer;


import com.accumed.msls.model.Account;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author AKhalifa
 * class for implements login, fetch insaurance card information for whealth client.
 */
public class WhealthClient extends Client {

    private static final String LOGIN_URL = "https://www.whealth-international.org/W.aspx";
    private static final String CARD_URL = "https://www.whealth-international.org/Provider/PreAuthorization1.aspx";

    public WhealthClient() {
        this(null);
    }

    public WhealthClient(Account account) {
        super(account);
    }

    public static WhealthClient getInstance(Account account) throws Exception {
        WhealthClient instance = new WhealthClient(account);
        instance.login();
        return instance;
    }

    /**
     * function for login to whealth portal.
     *
     * @return boolean of login state
     * @author AKhalifa
     */
    public boolean login() {
        long mi = new Date().getTime();
        boolean loginState = false;
        Logger.getLogger(WhealthClient.class.getName()).log(Level.INFO, "start login whealth client at : {0}", new Date());
        try {
            this.driver.navigate().to(LOGIN_URL);
            if (this.driver.findElements(By.id("txt_UserNamee")).size() != 0 && this.driver.findElements(By.id("txt_Passwordd")).size() != 0) {
                this.webService.login(this.driver, "", "txt_UserNamee", account.getUser(), "txt_Passwordd", account.getPass(), 1, "btn_Logon", 0);

                WebDriverWait wait = new WebDriverWait(this.driver, 3);
                try {
                    if (wait.until(ExpectedConditions.alertIsPresent()) != null) {
                        Alert alert = this.driver.switchTo().alert();
                        if (alert.getText().equals("Please check username and password")) {
                            Logger.getLogger(AlBuhairaClient.class.getName()).log(Level.INFO, "{0} ,...** Finish login Whealth client Please check username and password ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                            this.driver.quit();
                        }
                    }
                } catch (Exception ex) {
                    this.driver.navigate().to(CARD_URL);
                    loginState = true;
                    Logger.getLogger(WhealthClient.class.getName()).log(Level.INFO, "Finish login whealth client at time : {0}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            this.driver.quit();
        }
        return loginState;
    }

    @Override
    public InsuranceCard getCardByEId(String eId) {
        InsuranceCard card = null;
        long mi = new Date().getTime();
        Logger.getLogger(WhealthClient.class.getName()).log(Level.INFO, "Start fetching whealth card info at : {0} ", new Date());
        try {
            new WebDriverWait(this.driver, 10)
                    .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("form1")));
            if (this.driver.findElements(By.id("txt_memberEI")).size() != 0) {
                this.driver.findElement(By.id("txt_memberEI")).sendKeys(eId);
                this.driver.findElement(By.id("txtUidNumber")).click();
                Thread.sleep(2000);
                //check if member is correct or not
                if (this.driver.findElements(By.id("lbl_error")).size() != 0)
                    if (this.driver.findElement(By.id("lbl_error")).getText().equals("Member is not covered in this Network!")) {
                        card = new InsuranceCard("Member is not covered in this Network!", -1);
                        Logger.getLogger(WhealthClient.class.getName()).log(Level.INFO, "finish fetching wealth card Member is not covered in this Network! : {0}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                    }
                //fetch customer insurance card information.
                card = new InsuranceCard();
                //get name drop down list element.
                Select ddlMemberName = new Select(this.driver.findElement(By.id("ddl_memberName")));
                ddlMemberName.selectByIndex(1);
                Thread.sleep(2000);
                card.setMemberId(this.driver.findElement(By.id("txt_princiID")).getAttribute("value"));
                card.setName(this.driver.findElement(By.id("txt_priName")).getAttribute("value"));
                card.setDateOfBirth(this.driver.findElement(By.id("txt_DOB")).getAttribute("value"));
                card.setStartDate(this.driver.findElement(By.id("txt_pStartDate")).getCssValue("value"));
                card.setExpiryDate(this.driver.findElement(By.id("txt_pEndDate")).getAttribute("value"));
                card.setPayer(this.driver.findElement(By.id("txt_Client1")).getAttribute("value"));

                this.webService.changeFrame(this.driver, "//*[@id=\"MyIframe\"]", "MyIframe");
                this.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
                WebElement mainCopayTable = this.driver.findElement(By.id("Datalist7_DatalistA7_0"));
                WebElement copayTable = mainCopayTable.findElement(By.tagName("table"));
                List<WebElement> elementRecords = copayTable.findElements(By.tagName("tr"));

                List<WebElement> elementLabels = elementRecords.get(0).findElements(By.tagName("td"));

                String copay = "";
                for (int j = 1; j < elementRecords.size(); j++) {
                    List<WebElement> currentColumn = elementRecords.get(1).findElements(By.tagName("td"));

                    copay = "Deductible:[ " + currentColumn.get(0).getText().replaceAll("\n", "").trim() + " ],";
                    for (int i = 1; i < elementLabels.size(); i++) {
                        copay += elementLabels.get(i).getText().replaceAll("\n", "").trim() + "[ " + currentColumn.get(i).getText().replaceAll("\n", "").trim() + " ], ";
                    }
                    // for next row if exist.
                    copay = copay.substring(copay.length() - 1) + "\n";
                }
                card.setCopay(copay);
                card.setMessage(this.driver.findElement(By.xpath("//*[@id=\"Datalist7\"]/tbody/tr/td[1]/table[1]/tbody/tr[1]/td/table/tbody/tr[11]/td")).getText());
                card.setSuccess(1);

                Logger.getLogger(WhealthClient.class.getName()).log(Level.INFO, "finish fetching whealth card info for name : {0} at : {1} .", new Object[]{card.getName(), new Date(), (new Date().getTime() - mi) / 1000,});
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            card = new InsuranceCard("Whealth unknown error : {0}", -1);
            Logger.getLogger(WhealthClient.class.getName()).log(Level.INFO, "finish fetching whealth card info unknown error : {0} .", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
            //  this.driver.quit();
        }
        //this.driver.close();

        return card;
    }

    @Override
    public InsuranceCard getCardByEId(String eId, String encType) {
        return getCardByEId(eId);
    }

    @Override
    public boolean isValid() throws InterruptedException {
        this.driver.navigate().to(CARD_URL);
        if (this.driver.getCurrentUrl().equals(LOGIN_URL)) {
            if (login())
                return true;
            return false;
        }
        return true;
    }

    @Override
    public void destroyDrive() {
        this.driver.quit();
    }


}
