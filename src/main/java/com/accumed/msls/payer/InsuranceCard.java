/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.payer;


import com.accumed.msls.exception.HtmlParsingException;
import com.accumed.msls.exception.InvalidArguments;
import com.accumed.msls.exception.TimeoutException;

import java.io.Serializable;

/**
 *
 * @author amassri
 */
public class InsuranceCard implements Serializable {

    private String memberId;
    private String gender;
    private String expiryDate;
    private String benefits;
    private String plan;
    private String name;
    private String address;
    private String dateOfBirth;
    private String payer;
    private String message;
    private String copay;
    private String packageName;
    private String authNo;
    private String tpa;
    private String insurers;
    private String startDate;
    private Integer success;

    private String status;
    private String roomType;
    private String inpatient;
    private String consultation;
    private String pharmacy;
    private String lab_Radiology;
    private String routine_Dental;

    public InsuranceCard() {
    }

    public InsuranceCard(Exception e) {
        if (e instanceof TimeoutException) {
            this.success = -4;
        } else if (e instanceof InvalidArguments) {
            this.success = -6;
        } else if (e instanceof HtmlParsingException) {
            this.success = -7;
        } else {
            //Exception
            this.success = -1;
        }
        this.message = e.getClass().getName()+": "+e.getMessage();
    }

    public InsuranceCard(String message, Integer success) {
        this.message = message;
        this.success = success;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getInsurers() {
        return insurers;
    }

    public void setInsurers(String insurers) {
        this.insurers = insurers;
    }

    public String getTpa() {
        return tpa;
    }

    public void setTpa(String tpa) {
        this.tpa = tpa;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAuthNo() {
        return authNo;
    }

    public void setAuthNo(String authNo) {
        this.authNo = authNo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getBenefits() {
        return benefits;
    }

    public void setBenefits(String benefits) {
        this.benefits = benefits;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCopay() {
        return copay;
    }

    public void setCopay(String copay) {
        this.copay = copay;
    }

//    public Date getCardExpiryDate() {
//        Date expiryDateTemp = null;
//        if (this.getExpiryDate() != null) {
//            expiryDateTemp = JsfUtil.parseExpiryDate(getExpiryDate());
//        }
//        return expiryDateTemp;
//    }
//    Encounter enc;
//
//    public void setEnc(Encounter enc) {
//        this.enc = enc;
//    }
//    public boolean isCardExpired() {
//
//        if (enc.getStartDate() != null && getCardExpiryDate() != null) {
//            return getCardExpiryDate().compareTo(JsfUtil.parseDateWithPattern(JsfUtil.formatDate(enc.getStartDate(), "dd/MM/yyyy"), "dd/MM/yyyy")) < 0;
//        }
//        return false;
//    }
    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getInpatient() {
        return inpatient;
    }

    public void setInpatient(String inpatient) {
        this.inpatient = inpatient;
    }

    public String getConsultation() {
        return consultation;
    }

    public void setConsultation(String consultation) {
        this.consultation = consultation;
    }

    public String getPharmacy() {
        return pharmacy;
    }

    public void setPharmacy(String pharmacy) {
        this.pharmacy = pharmacy;
    }

    public String getLab_Radiology() {
        return lab_Radiology;
    }

    public void setLab_Radiology(String lab_Radiology) {
        this.lab_Radiology = lab_Radiology;
    }

    public String getRoutine_Dental() {
        return routine_Dental;
    }

    public void setRoutine_Dental(String routine_Dental) {
        this.routine_Dental = routine_Dental;
    }

}
