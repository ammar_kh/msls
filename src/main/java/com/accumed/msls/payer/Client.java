package com.accumed.msls.payer;

import com.accumed.msls.model.Account;
import com.accumed.msls.service.WebService;
import com.accumed.msls.service.WebServiceImp;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author AKhalifa
 * main core abstract class that will use as a generic class for other client.
 */
public abstract class Client {
    private static final ThreadLocal<WebDriver> WEB_DRIVER_THREAD_LOCAL = new ThreadLocal<WebDriver>() {
        @Override
        protected WebDriver initialValue() {
            return new ChromeDriver();
        }
    };
    //define object of selenium web driver
    Map<String, ChromeDriver> driverList = new HashMap<>();

    // WebDriver driver;
    ChromeDriver driver;
    //define object of account.
    Account account;

    protected WebService webService = new WebServiceImp();
    private static final AtomicLong NEXT_ID = new AtomicLong(0);
    final long uniqueId = NEXT_ID.getAndIncrement();

    public Client(Account account) {

        //initialize system property by chrome web driver
        String WEBDRIVER_CHROME_PATH = "../resources/chromedriver/chromedriver.exe";
        try {
            File f = new File(WEBDRIVER_CHROME_PATH);
            String absolutePath = f.getAbsolutePath();
            if (absolutePath.contains("bin")) {
                WEBDRIVER_CHROME_PATH = "../resources/chromedriver/chromedriver.exe";
            } else {
                WEBDRIVER_CHROME_PATH = "resources/chromedriver/chromedriver.exe";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.setProperty("webdriver.chrome.driver", WEBDRIVER_CHROME_PATH);
        //set driver as chromeDriver
        if (this.driver == null) {
            Logger.getLogger(AlBuhairaClient.class.getName()).log(Level.INFO, " client start driver at {0}", new Date());
            ChromeOptions options = new ChromeOptions();
//            DesiredCapabilities capabilities = new DesiredCapabilities();
//            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//            options.merge(capabilities);
            //  options.addArguments("--headless");
            Map prefs = new HashMap();
            prefs.put("profile.managed_default_content_settings.images", 2);
//            prefs.put("profile.managed_default_content_settings.javascript", 2);
            prefs.put("profile.managed_default_content_settings.stylesheet", 2);
            options.setPageLoadStrategy(PageLoadStrategy.EAGER);
            options.setExperimentalOption("prefs", prefs);
            options.addArguments("start-maximized");
            this.driver = new ChromeDriver(options);

            //this.driver = WEB_DRIVER_THREAD_LOCAL.get();
            //   _driver.
            //maximize window of chrome drive.
            //this.driver.manage().window().maximize();
        }
        //initialize account for client.
        System.out.println(this.driver.getSessionId());
        this.account = account;
    }

    public abstract boolean login() throws Exception;

    public abstract InsuranceCard getCardByEId(String eId) throws InterruptedException, IOException;

    public abstract InsuranceCard getCardByEId(String eId, String encType) throws InterruptedException, IOException;

    public abstract boolean isValid() throws InterruptedException;

    public long getUniqueId() {
        return this.uniqueId;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public void disableCss() {
        JavascriptExecutor js = (JavascriptExecutor) this.driver;
        js.executeScript("if(document.head!=null){document.head.parentNode.removeChild(document.head);}");
    }

    protected void refresh(String url) {
        if (this.driver.getCurrentUrl().equals(url)) {
            this.driver.navigate().refresh();
        } else {
            this.driver.navigate().to(url);
        }
    }

    protected abstract void destroyDrive();
}
