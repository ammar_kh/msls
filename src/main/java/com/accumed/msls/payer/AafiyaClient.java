/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.payer;


import com.accumed.msls.model.Account;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Anas
 */
public class AafiyaClient extends Client {

    public AafiyaClient(Account account) {
        super(account);
        this.driver.navigate().to("https://tpa.aafiya.ae/AafiyaTPA/");
    }

    public AafiyaClient() {
        this(null);
    }

    public static AafiyaClient getInstance(Account account) throws Exception {
        AafiyaClient instance = new AafiyaClient(account);
        instance.login();
        return instance;
    }

    public boolean login() throws IOException, Exception {
        long mi = new Date().getTime();
        Logger.getLogger(AafiyaClient.class.getName()).log(Level.INFO, new Date() + "...** Start login Aafiya CLient ...");
        disableCss();
        this.driver.findElement(By.xpath("//input[@id='UserName']")).sendKeys(this.account.getUser());
        this.driver.findElement(By.xpath("//input[@id='Password']")).sendKeys(this.account.getPass());
        this.driver.findElement(By.xpath("//button[@id='LoginSubmit']")).click();
        Thread.sleep(5000);

        Logger.getLogger(AafiyaClient.class.getName()).log(Level.INFO, new Date() + " ,...** Finish login Aafiya CLient ... Time(s):" + ((new Date().getTime() - mi) / 1000));
        return true;
    }

    @Override
    public InsuranceCard getCardByEId(String eId) throws IOException, InterruptedException {
        return getCardByEID(eId);
    }

    @Override
    public InsuranceCard getCardByEId(String eId, String encType) {
        return getCardByEId(eId, "");
    }

    @Override
    public boolean isValid() throws InterruptedException {
        return false;
    }

    @Override
    public void destroyDrive() {
        System.out.println("aafiya evict with chrome driver session id = "+this.driver.getSessionId());

        this.driver.quit();
    }

    public InsuranceCard getCardByEID(String eid) throws IOException, InterruptedException {
        InsuranceCard card = null;
        Logger.getLogger(AafiyaClient.class.getName()).log(Level.INFO, new Date() + " ...** Start fetching Aafiya card info ..");
        long mi = new Date().getTime();

        try {
            this.driver.navigate().to("https://tpa.aafiya.ae/AafiyaTPA/MemberInformation#/:-%7C");
            // disableCss();
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='SelectionType']")));
            List<WebElement> SelectionType = this.driver.findElements(By.xpath("//select[@id='SelectionType']/*"));
            SelectionType.get(2).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='SearchParam']")));
            WebElement SearchParam = this.driver.findElement(By.xpath("//input[@id='SearchParam']"));
            SearchParam.sendKeys(eid);
            //JavascriptExecutor js = (JavascriptExecutor) driver;
            //String s = "FindMember();";
            //js.executeAsyncScript(s);
            String search_xpath = "/html/body/div[2]/section/div/div/div/div/div[2]/div[1]/div[3]/button";
            this.driver.findElement(By.xpath(search_xpath)).click();
            Thread.sleep(1000);
            if (this.driver.findElements(By.xpath("//div[@class='uk-notify uk-notify-top-center']")).size() != 0) {
                WebElement status = this.driver.findElement(By.xpath("//div[@class='uk-notify uk-notify-top-center']"));
                System.out.println(status.getText());
                if (status != null) {
                    card = new InsuranceCard("Not Found: Member Not Found !!", -2);
                    Logger.getLogger(AafiyaClient.class.getName()).log(Level.INFO, new Date() + " ,...** Finish fetching Aafiya CLient ... Time(s):" + ((new Date().getTime() - mi) / 1000));
                    return card;
                }
            } else {
                card = new InsuranceCard();

                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='table  table-striped ng-table-responsive infotable']")));
                WebElement table1 = this.driver.findElement(By.xpath("/html/body/div[4]/div/div/div[2]/div/div/div[1]/div/div/div[3]/table/tbody"));
                WebElement table1_MemberId_element = table1.findElement(By.xpath("tr[1]/td[2]"));
                WebElement table1_Name_element = table1.findElement(By.xpath("tr[2]/td[2]"));
                WebElement table1_ExpiryDate_element = table1.findElement(By.xpath("tr[4]/td[2]"));
                WebElement table1_StartDate_element = table1.findElement(By.xpath("tr[5]/td[2]"));
                WebElement table1_Payer_element = table1.findElement(By.xpath("tr[7]/td[2]"));
                WebElement table1_Message_element = table1.findElement(By.xpath("tr[8]/td[2]"));
                card.setMemberId(table1_MemberId_element.getText().replaceAll("\n", "").trim().replaceAll("[^\\d]", ""));
                card.setName(table1_Name_element.getText().replaceAll("\n", "").trim());
                card.setExpiryDate(table1_ExpiryDate_element.getText().replaceAll("\n", "").trim());
                card.setStartDate(table1_StartDate_element.getText().replaceAll("\n", "").trim());
                card.setPayer(table1_Payer_element.getText().replaceAll("\n", "").trim());
                card.setMessage(table1_Message_element.getText().replaceAll("\n", "").trim());
                //tabel2
                WebElement table2 = this.driver.findElement(By.xpath("/html/body/div[4]/div/div/div[2]/div/div/div[1]/div/div/div[4]/table/tbody"));
                WebElement table2_Gender_element = table2.findElement(By.xpath("tr[2]/td[2]"));
                WebElement table2_Plan_element = table2.findElement(By.xpath("tr[2]/td[2]"));
                WebElement table2_DateOfBirth_element = table2.findElement(By.xpath("tr[2]/td[2]"));
                card.setGender(table2_Gender_element.getText().replaceAll("\n", "").trim());
                card.setPlan(table2_Plan_element.getText().replaceAll("\n", "").trim());
                card.setDateOfBirth(table2_DateOfBirth_element.getText().replaceAll("\n", "").trim());

                //tabel3
                String copay = "";
                List<WebElement> table3 = this.driver.findElements(By.xpath("/html/body/div[4]/div/div/div[2]/div/div/div[1]/div/div/div[8]/table/tbody/*"));
                for (WebElement tr : table3) {
                    List<WebElement> tds = tr.findElements(By.xpath("./*"));
                    copay += tds.get(0).getText().replaceAll("\n", "").trim() + " : [" + tds.get(1).getText().replaceAll("\n", "").trim() + "],";
                }
                //tabel4
                List<WebElement> table4 = this.driver.findElements(By.xpath("/html/body/div[4]/div/div/div[2]/div/div/div[1]/div/div/div[9]/table/tbody/*"));

                for (WebElement tr : table4) {
                    List<WebElement> tds = tr.findElements(By.xpath("./*"));
                    copay += tds.get(0).getText().replaceAll("\n", "").trim() + " : [" + tds.get(1).getText().replaceAll("\n", "").trim() + "],";
                }

                //tabel5
                List<WebElement> table5 = this.driver.findElements(By.xpath("/html/body/div[4]/div/div/div[2]/div/div/div[1]/div/div/div[11]/table/tbody/*"));
                copay += " CopaymentService:{ ";
                for (WebElement tr : table5) {
                    String str
                            = "{ [Co-Insurance Inside UAE(%)]= [" + tr.findElement(By.cssSelector("td[data-title=\"'Co-Insurance Inside UAE(%)'\"]")).getText() + "],"
                            + "[Deductible Inside UAE]= [" + tr.findElement(By.cssSelector("td[data-title=\"'Deductible Inside UAE'\"]")).getText() + "],"
                            + "[Co-Insurance Outside UAE(%)]= [" + tr.findElement(By.cssSelector("td[data-title=\"'Co-Insurance Outside UAE(%)'\"]")).getText() + "],"
                            + "[Deductible Outside UAE]= [" + tr.findElement(By.cssSelector("td[data-title=\"'Deductible Outside UAE'\"]")).getText() + "],"
                            + "[Co-Insurance Outside Network(%)]= [" + tr.findElement(By.cssSelector("td[data-title=\"'Co-Insurance Outside Network(%)'\"]")).getText() + "],"
                            + "[Deductible Outside Network]= [" + tr.findElement(By.cssSelector("td[data-title=\"'Deductible Outside Network'\"]")).getText() + "]},";
                    copay += str;
                }
                copay += " }";
                card.setCopay(copay);
                //2
                List<WebElement> table6 = this.driver.findElements(By.xpath("/html/body/div[4]/div/div/div[2]/div/div/div[1]/div/div/div[13]/div[2]/table/tbody/*"));
                String benefits = " Benefits:{ ";
                for (WebElement tr : table6) {
                    String str
                            = "{ [CopaymentService]= [" + tr.findElement(By.cssSelector("td[data-title=\"'CopaymentService'\"]")).getText() + "],"
                            + "[Apply For]= [" + tr.findElement(By.cssSelector("td[data-title=\"'Apply For'\"]")).getText() + "],"
                            + "[FOB]= [" + tr.findElement(By.cssSelector("td[data-title=\"'FOB'\"]")).getText() + "],"
                            + "[Max Co-pay Inside UAE]= [" + tr.findElement(By.cssSelector("td[data-title=\"'Max Co-pay Inside UAE'\"]")).getText() + "],"
                            + "[Max Co-pay Outside Network]= [" + tr.findElement(By.cssSelector("td[data-title=\"'Max Co-pay Outside Network'\"]")).getText() + "],"
                            + "[Max Co-pay Outside UAE]= [" + tr.findElement(By.cssSelector("td[data-title=\"'Max Co-pay Outside UAE'\"]")).getText() + "],"
                            + "[Aggregate CAP]= [" + tr.findElement(By.cssSelector("td[data-title=\"'Aggregate CAP'\"]")).getText() + "]},";
                    benefits += str;
                }
                benefits += " }";
                card.setBenefits(benefits);
                card.setSuccess(1);
                Logger.getLogger(AafiyaClient.class.getName()).log(Level.INFO, new Date() + " ,...** Finish fetching Aafiya CLient ... Time(s):" + ((new Date().getTime() - mi) / 1000));
                this.driver.findElement(By.xpath("//*[@id=\"MemberSysteminfoDetailsID\"]/div/div[3]/button[5]")).click();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            card = new InsuranceCard("Aafiya unknown error", -1);
        }
        return card;

    }
}
