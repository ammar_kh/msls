package com.accumed.msls.payer;


import com.accumed.msls.model.Account;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAMANClient extends Client {
    private static final String LOGIN_URL = "https://www.damanhealth.ae/en";
    private static final String CARD_URL = "https://www.damanhealth.ae/eDamanApp/loadMyPortalPage.action";


//    public DAMANClient() {
//        this(null);
//    }

    public DAMANClient(Account account) {
        super(account);
    }

    public static DAMANClient getInstance(Account account) throws Exception {
        DAMANClient instance = new DAMANClient(account);
        instance.login();
        return instance;
    }

    /**
     * @author AKhalifa
     * login function for almadallah client
     * the login button is not clickable, so we change the element class cause we need force view login form.
     */
    @Override
    public boolean login() {
        long mi = new Date().getTime();
        boolean loginState = false;
        Logger.getLogger(DAMANClient.class.getName()).log(Level.INFO, new Date() + "{0}...** Start login Daman Client ...");

        try {
            this.driver.navigate().to(LOGIN_URL);
            disableCss();
            //change class for element
            WebElement elem = this.driver.findElement(By.id("sign-in-form"));
            String js = "arguments[0].style.height='auto'; arguments[0].setAttribute('class', 'collapse in')";

            if (this.driver.findElements(By.id("j_username")).size() != 0 && this.driver.findElements(By.id("j_password")).size() != 0) {
                this.webService.login(this.driver, "", "j_username", account.getUser(), "j_password", account.getPass(), 4, "btn-sign-in", 2500);

                if (this.driver.findElements(By.className("notification_msg")).size() != 0) {
                    if (this.driver.findElement(By.className("notification_msg")).getText().equals("Please enter a valid user name and password")) {
                        Logger.getLogger(DAMANClient.class.getName()).log(Level.INFO, new Date() + "{0}...** login Daman Client is  Incorrect UserName or Password...");
                    }
                } else {
                    this.driver.navigate().to(CARD_URL);
                    if (this.driver.getCurrentUrl().equals(CARD_URL)) {
                        Logger.getLogger(DAMANClient.class.getName()).log(Level.INFO, "{0} , ...** finish login Daman client ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                        loginState = true;
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(DAMANClient.class.getName()).log(Level.INFO, new Date() + "Daman unknown login error");
            this.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        }
        return loginState;
    }

    @Override
    public InsuranceCard getCardByEId(String eId) throws InterruptedException {
        InsuranceCard card = null;
        long mi = new Date().getTime();
        Logger.getLogger(DAMANClient.class.getName()).log(Level.INFO, "*start fetching Daman card information");

        refresh(CARD_URL);
        disableCss();
        try {
            if (this.driver.findElements(By.cssSelector("input[value='EID']")).size() != 0) {
                this.driver.findElement(By.cssSelector("input[value='EID']")).click();
                if (this.driver.findElements(By.id("eid1")).size() != 0 && this.driver.findElements(By.id("eid2")).size() != 0 && this.driver.findElements(By.id("eid3")).size() != 0 && this.driver.findElements(By.id("eid4")).size() != 0) {
                    this.driver.findElement(By.id("eid2")).sendKeys(eId.substring(4, 8));
                    this.driver.findElement(By.id("eid3")).sendKeys(eId.substring(9, 16));
                    this.driver.findElement(By.id("eid4")).sendKeys(eId.substring(eId.length() - 1));
                    this.driver.findElement(By.id("mv_search")).click();
                    Thread.sleep(1500);
                    // if page display error message will process as below.
                    if (this.driver.findElements(By.cssSelector("div[class='alert alert-danger']")).size() != 0) {
                        //define error message container
                        WebElement errorMsg = this.driver.findElement(By.className("notification_msg"));
                        if (errorMsg.getText().startsWith("Status: \"Not Eligible\"")) {
                            card = new InsuranceCard("Status Not Eligible", -1);
                            Logger.getLogger(DAMANClient.class.getName()).log(Level.INFO, "{0} ,...** Finish fetching Daman Client ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                        } else if (errorMsg.getText().equals("The Emirates ID number entered is incorrect. Please check and try again.")) {
                            card = new InsuranceCard("The Emirates ID number entered is incorrect", -1);
                            Logger.getLogger(THIQAClient.class.getName()).log(Level.INFO, "{0} ,...** Finish fetching Daman Client ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                        }
                    } else {
                        //check if card id is valid and card displayed
//                        WebDriverWait wait = new WebDriverWait(this.driver, 10);
//                        wait.until(ExpectedConditions.visibilityOf(this.driver.findElement(By.className("card"))));
//                        Thread.sleep(2000);
                        if (this.driver.findElements(By.className("card")).size() != 0) {

                            card = new InsuranceCard();
                            card.setPlan(this.driver.findElements(By.cssSelector("div[class='form-section card-layout']")).get(0).getText());
                            card.setMemberId(this.driver.findElement(By.cssSelector("div[class='cardnumber planname-oncard']")).getText());
                            card.setGender(this.driver.findElement(By.xpath("//*[@id=\"card_response_div\"]/div/div[1]/div/div[2]/div/div[1]/div[1]/p")).getText());
                            card.setDateOfBirth(this.driver.findElement(By.xpath("//*[@id=\"card_response_div\"]/div/div[1]/div/div[2]/div/div[1]/div[2]/p")).getText());
                            card.setExpiryDate(this.driver.findElement(By.xpath("//*[@id=\"card_response_div\"]/div/div[1]/div/div[2]/div/div[2]/div[1]/p")).getText());

                            WebElement element = this.driver.findElement(By.className("cardborder2"));

                            Thread.sleep(300);
                            card.setName(element.findElements(By.tagName("h2")).get(0).getText());
                            card.setAddress(element.findElements(By.tagName("h2")).get(1).getText());
                            //initialize benefit url
                            String benefitUrl = "https://www.damanhealth.ae/eDamanApp/";

                            WebElement benefitElement = this.driver.findElement(By.xpath("//*[@id=\"card_response_div\"]/div/div[1]/div/div[2]/div/div[2]/div[2]/ul/li[1]/a"));

                            benefitUrl += this.driver.findElement(By.xpath("//*[@id=\"card_response_div\"]/div/div[1]/div/div[2]/div/div[2]/div[2]/ul/li[1]/a")).getAttribute("href").substring(30, benefitElement.getAttribute("href").length() - 14);

                            card.setBenefits(benefitUrl);

                            card.setSuccess(1);
                            Thread.sleep(200);
                            Logger.getLogger(DAMANClient.class.getName()).log(Level.INFO, new Date() + " , Finish fetching card Daman CLient ... Time(s):" + ((new Date().getTime() - mi) / 1000) + " , Name = " + card.getName());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            card = new InsuranceCard("Daman unknown error", -1);
            Logger.getLogger(DAMANClient.class.getName()).log(Level.INFO, new Date() + " , Finish fetching card Daman CLient ... Time(s):" + ((new Date().getTime() - mi) / 1000) + " , Name = " + card.getName());
            isValid();
        }
        //   Thread.sleep(1000);
        //  this.driver.quit();
        return card;
    }

    @Override
    public InsuranceCard getCardByEId(String eId, String encType) throws InterruptedException {
        return getCardByEId(eId);
    }

    @Override
    public boolean isValid() throws InterruptedException {
        this.driver.navigate().refresh();
        if (this.driver.getCurrentUrl().equals(LOGIN_URL)) {
            this.driver.navigate().to(CARD_URL);
            if (this.driver.getCurrentUrl().equals(LOGIN_URL)) {
                if (login())
                    return true;
                return false;
            }
        }
        return true;
    }


    public void destroyDrive() {
        System.out.println("daman evict with chrome driver session id = "+this.driver.getSessionId());
        this.driver.quit();
    }

}
