package com.accumed.msls.payer;


import com.accumed.msls.model.Account;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Author AKhalifa
 */
public class ADNICClient extends Client {
    private static final String ADNIC_LOGIN_URL = "https://online.adnic.ae/eportal/login.html";
    private static final String PROVIDER_URL = "https://online.adnic.ae/eportal/providers.html";
    private static final String PARTNER_URL = "https://online.adnic.ae/eportal/partner.html";

    public ADNICClient() {
        this(null);
    }

    public ADNICClient(Account account) {
        super(account);
    }

    public static ADNICClient getInstance(Account account) throws Exception {
        ADNICClient instance = new ADNICClient(account);
        instance.login();
        return instance;
    }

    /**
     * @author AKhalifa
     * call adnic login function to login to portal page.
     */
    @Override
    public boolean login() throws InterruptedException {
        long mi = new Date().getTime();
        boolean loginState = false;

        Logger.getLogger(ADNICClient.class.getName()).log(Level.INFO, new Date() + "{0}...** Start login ADNIC Client ...");

        this.webService.login(this.driver, ADNIC_LOGIN_URL, "userIdEcommerce", this.account.getUser(), "pwdEcommerce", this.account.getPass(), 3, "//*[@id=\"HomeLogIn\"]/form/a", 2500);

        try {
            //check if html open  popup alert.
            //if another session is open then open new one.
            if (this.driver.findElements(By.id("alertify")).size() != 0) {
                WebElement message = this.driver.findElement(By.id("alertify"));
                if (message.findElement(By.className("alertify-message")).getText().equals("User already login from other system. Do you want to logout that session?")) {
                    //session is expire
                    Logger.getLogger(ADNICClient.class.getName()).log(Level.INFO, new Date() + " ,...** Adnic login open new session ... Time(s):" + ((new Date().getTime() - mi) / 1000));
                    this.driver.findElement(By.id("alertify-ok")).click();

                    Thread.sleep(1500);


                    Logger.getLogger(ADNICClient.class.getName()).log(Level.INFO, new Date() + " ,...** Finish login ADNIC Client ... Time(s):" + ((new Date().getTime() - mi) / 1000));
                    loginState = true;
                }

                // check if invalid user id /user name or password incorrect/
                else if (message.findElement(By.className("alertify-message")).getText().equals("Invalid User Id")) {
                    Logger.getLogger(ADNICClient.class.getName()).log(Level.INFO, new Date() + " ,...** Adnic login invalid user id ... Time(s):" + ((new Date().getTime() - mi) / 1000));
                    this.driver.findElement(By.id("alertify-ok")).click();
                    return false;
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(ADNICClient.class.getName()).log(Level.INFO, new Date() + "adnic unknown login error");
            loginState = false;
        }
        return loginState;
    }

    /**
     * @param eId that will use for  check from adnic client
     *            get adnic card information by eId
     * @author by akhalifa
     */
    @Override
    public InsuranceCard getCardByEId(String eId) throws InterruptedException {
        InsuranceCard card = null;
        Logger.getLogger(ADNICClient.class.getName()).log(Level.INFO, new Date() + " *start fetching adnic card information");
        long mi = new Date().getTime();
        try {
            if (checkIfSessionExpired())
                login();
            refresh(PROVIDER_URL);
            disableCss();

            this.webService.changeFrame(this.driver, "//*[@id=\"providerAdnicFrame\"]", "providerAdnicFrame");

            this.driver.findElement(By.xpath("//*[@id=\"mOnlineEligiblity\"]/a"))
                    .click();

            Thread.sleep(1200);

            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("txtEmiratesId"))));
            //check if session is expire.
            if (checkIfSessionExpired()) {
                login();
                getCardByEId(eId);
            } else {
                this.webService.changeFrame(this.driver, "//*[@id=\"providerAdnicFrame\"]", "providerAdnicFrame");
            }

            //get Adnic Card Information
            this.driver.findElement(By.id("txtEmiratesId")).sendKeys(eId);
            Thread.sleep(100);
            this.driver.findElement(By.id("btnSearch")).click();
            this.driver.switchTo().defaultContent();

            //check if eId is invalid

            if (this.driver.findElements(By.id("alertify")).size() != 0) {
                // check if invalid massage is display

                if (this.driver.findElement(By.className("alertify-message")).getText().equals("Given Member ID/Emirates ID is invalid")) {
                    card = new InsuranceCard("Given Member ID/Emirates ID is invalid", -2);
                    Logger.getLogger(ADNICClient.class.getName()).log(Level.INFO, "{0} ,...** Finish fetching Adnic CLient ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                    this.driver.findElement(By.id("alertify-ok")).click();

                    this.webService.changeFrame(this.driver, "//*[@id=\"providerAdnicFrame\"]", "providerAdnicFrame");

                    return card;
                }

            } else {
                //fill card of adnic client
                this.webService.changeFrame(this.driver, "//*[@id=\"providerAdnicFrame\"]", "providerAdnicFrame");
                Thread.sleep(1000);

                wait.until(ExpectedConditions.visibilityOf(this.driver.findElement(By.id("divResult"))));
                //fill data of adnic card
                card = new InsuranceCard();
                card.setMemberId(this.driver.findElement(By.id("lblMemberId")).getText());
                card.setDateOfBirth(this.driver.findElement(By.id("lblDob")).getText());
                card.setGender(this.driver.findElement(By.id("lblGender")).getText());
                card.setName(this.driver.findElement(By.id("lblMemberName")).getText());
                card.setExpiryDate(this.driver.findElement(By.id("lblExpDate")).getText());
                card.setTpa(this.driver.findElement(By.id("lblNetworkType")).getText());

                WebElement tabElement = this.driver.findElement(By.id("tab2"));
                if (tabElement != null)
                    if (tabElement.isDisplayed()) {
                        tabElement.click();
                        //check if session is expire
                        if (checkIfSessionExpired()) {
                            login();
                            getCardByEId(eId);
                        } else {
                            this.webService.changeFrame(this.driver, "//*[@id=\"providerAdnicFrame\"]", "providerAdnicFrame");
                            disableCss();
                        }

                        WebElement element = this.driver.findElement(By.id("tabcontent2"));
                        if (element.isDisplayed()) {
                            Thread.sleep(1000);
                            WebElement wElement = element.findElement(By.xpath("//div[@class='accordian']"));
                            if (wElement != null) {
                                List<WebElement> elements = wElement.findElements(By.xpath("//div[@class='panel panel-default']"));
                                if (elements.size() > 0) {
                                    String ben = "Benefits { ";
                                    for (WebElement acc : elements) {
                                        ben += acc.findElement(By.tagName("h4")).getText() + ": \n";
                                        acc.click();
                                        WebElement ulElement = acc.findElement(By.tagName("ul"));
                                        List<WebElement> liElements = ulElement.findElements(By.tagName("li"));
                                        if (liElements.size() > 0) {
                                            //Thread.sleep(200);
                                            if (liElements.size() == 1)
                                                ben += " [" + liElements.get(0).getText() + "] \n";
                                            else if (liElements.size() > 1) {
                                                for (WebElement li : liElements) {
                                                    ben += " [" + (" " + li.getText() + "],\n");
                                                    // Thread.sleep(50);
                                                }
                                                //delete last comma ,
                                                ben = ben.substring(0, ben.length() - 2) + "\n";
                                            }
                                        }
                                    }
                                    ben += " }";
                                    card.setBenefits(ben);
                                }
                            }
                        }
                    }
                card.setSuccess(1);
                Logger.getLogger(ADNICClient.class.getName()).log(Level.INFO, new Date() + " ,...** Finish fetching Adnic CLient ... Time(s):" + ((new Date().getTime() - mi) / 1000));
            }
            this.driver.navigate().refresh();

        } catch (Exception ex) {
            ex.printStackTrace();
            card = new InsuranceCard("adnic unknown error", -1);
            isValid();
        }
        return card;
    }

    @Override
    public InsuranceCard getCardByEId(String eId, String encType) throws InterruptedException {
        return getCardByEId(eId);
    }

    @Override
    public boolean isValid() throws InterruptedException {
        this.driver.navigate().to(PROVIDER_URL);
        //check if session is ended or driver back to login page then try login.
        if (checkIfSessionExpired() || this.driver.getCurrentUrl().equals(ADNIC_LOGIN_URL)) {
            if (login())
                return true;
            return false;
        }
        return true;
    }

    @Override
    public void destroyDrive() {
        this.driver.quit();
    }

    private boolean checkIfSessionExpired() {
        this.driver.switchTo().defaultContent();
        if (this.driver.findElements(By.id("alertify")).size() != 0) {
            if (this.driver.findElement(By.className("alertify-message")).getText().equals("User session expired. Kindly login!")) {
                this.driver.findElement(By.id("alertify-ok")).click();
                return true;
            }
        }
        return false;
    }
}
