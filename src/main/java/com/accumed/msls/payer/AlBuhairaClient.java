package com.accumed.msls.payer;


import com.accumed.msls.model.Account;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author AKhalifa
 * albuhairaClient class
 */

public class AlBuhairaClient extends Client {
    private static final String LOGIN_URL = "https://provider.albuhaira.com/";
    private static final String CARD_URL = "https://provider.albuhaira.com/MemberInformationSearch.aspx";


    public AlBuhairaClient() {
        this(null);
    }

    //default constructor of client.
    public AlBuhairaClient(Account account) {
        super(account);
    }


    public static AlBuhairaClient getInstance(Account account) throws InterruptedException {
        AlBuhairaClient instance = new AlBuhairaClient(account);
        instance.login();
        return instance;
    }


    /**
     * @author by AKhalifa
     * call albuhaira login function
     */
    public boolean login() throws InterruptedException {
        long mi = new Date().getTime();
        boolean loginState = false;

        Logger.getLogger(AlBuhairaClient.class.getName()).log(Level.INFO, new Date() + "{0}...** Start login Albuhaira Client ...");

        try {
            this.driver.navigate().to(LOGIN_URL);
            disableCss();
            WebElement loginBtn = this.driver.findElement(By.xpath("/html/body/header/nav/div/div[2]/ul/li[3]/a"));
//            WebDriverWait wait = new WebDriverWait(this.driver, 20);
//
//            wait.until(ExpectedConditions.visibilityOf(loginBtn));
            //Thread.sleep(5000);

            loginBtn.click();
            Thread.sleep(1000);


            this.webService.login(this.driver, "", "user-name", account.getUser(), "user-pass", account.getPass(), 1, "login_btn", 5000);

            try {
                Logger.getLogger(AlBuhairaClient.class.getName()).log(Level.INFO, new Date() + "{0}...** Finish login Albuhaira Client ...");

                this.driver.navigate().to(CARD_URL);
                //Thread.sleep(2000);
                loginState = true;
            } catch (Exception ex) {
                if (this.driver.switchTo().alert().getText().equals("Provider not found or username password invalid . Please contact Administrator !")) {
                    Logger.getLogger(AlBuhairaClient.class.getName()).log(Level.INFO, new Date() + "{0}...** login Albuhaira Client is invalid cause username password invalid...");

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(AlBuhairaClient.class.getName()).log(Level.INFO, new Date() + "{0}...** unknown Albuhaira login error...");
        }
        return loginState;
    }

    @Override
    public InsuranceCard getCardByEId(String eId) throws InterruptedException {
        return getCardById(eId);
    }

    @Override
    public InsuranceCard getCardByEId(String eId, String encType) {
        return null;
    }

    /**
     * @param cardId that will use for  check from adnic client
     *               get adnic card information by eId
     * @author by AKhalifa
     */
    public InsuranceCard getCardById(String cardId) throws InterruptedException {
        InsuranceCard card = null;
        Logger.getLogger(AlBuhairaClient.class.getName()).log(Level.INFO, new Date() + " *start fetching Albuhaira card information");
        long mi = new Date().getTime();
        refresh(CARD_URL);
        //  disableCss();
        try {

            WebDriverWait wait = new WebDriverWait(this.driver, 10);
//            wait.until(ExpectedConditions.visibilityOf(this.driver.findElement(By.id("TextBox1"))));
            // if (this.driver.findElements(By.id("TextBox1")).size() == 0)
            //  Thread.sleep(1000);

            WebElement eIdTarget = this.driver.findElement(By.id("TextBox1"));

            eIdTarget.clear();

            eIdTarget.sendKeys(cardId);
            this.driver.findElement(By.id("Button1")).click();
            Thread.sleep(4000);

            try {
                if (wait.until(ExpectedConditions.alertIsPresent()) != null) {
                    Alert alert = this.driver.switchTo().alert();
                    if (alert.getText().equals("Member not found or Card No invalid")) {
                        card = new InsuranceCard("Member not found or Card No invalid", -2);
                        Logger.getLogger(AlBuhairaClient.class.getName()).log(Level.INFO, "{0} ,...** Finish fetching Albuhaira Client ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                        alert.accept();
                    }
                }

            } catch (Exception ex) {
                String memberName = this.driver.findElement(By.id("txtMemberName")).getAttribute("value");

                if (!memberName.isEmpty()) {
                    card = new InsuranceCard();
                    card.setName(memberName);
                    card.setStatus(this.driver.findElement(By.id("txtStatus")).getAttribute("value"));
                    card.setPlan(this.driver.findElement(By.id("txtPlan")).getAttribute("value"));
                    card.setDateOfBirth(this.driver.findElement(By.id("txtDOB")).getAttribute("value"));
                    card.setStartDate(this.driver.findElement(By.id("txtVaildFrom")).getAttribute("value"));
                    card.setBenefits(this.driver.findElement(By.id("txtnetwork")).getAttribute("value"));
                    card.setExpiryDate(this.driver.findElement(By.id("txtVaildTo")).getAttribute("value"));
                    card.setCopay(this.driver.findElement(By.id("txtcardtext")).getAttribute("value"));
                    card.setMemberId(this.driver.findElement(By.id("txtCardId")).getAttribute("value"));
                    card.setSuccess(1);
                    Logger.getLogger(AlBuhairaClient.class.getName()).log(Level.INFO, "{0} , ...** Finish fetching  Albuhaira ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                    this.driver.navigate().refresh();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            card = new InsuranceCard("Albuhaira unknown error", -1);
            isValid();
        }
        // this.driver.quit();
        return card;
    }

    @Override
    public long getUniqueId() {
        return this.uniqueId;
    }

    @Override
    public void destroyDrive() {
        this.driver.quit();
    }

    /**
     * check if site valid for get information for eId.
     *
     * @return
     * @throws InterruptedException
     */
    @Override
    public boolean isValid() throws InterruptedException {
        this.driver.navigate().to(CARD_URL);
        if (this.driver.getCurrentUrl().equals(LOGIN_URL)) {
            if (login())
                return true;
            else
                return false;
        }
        return true;
    }
}
