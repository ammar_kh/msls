package com.accumed.msls.payer;


import com.accumed.msls.model.Account;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author AKhalifa
 * class for implements login, fetch almadallah inscurance card information for client
 */
public class ALMADALLAHClient extends Client {

    private static final String LOGIN_URL = "https://www.almadallah.ae/Account/Login.aspx";
    private static final String CARD_URL = "https://www.almadallah.ae/provider/MemberInfo.aspx";

    public ALMADALLAHClient() {
        this(null);
    }

    public ALMADALLAHClient(Account account) {
        super(account);
    }

    /**
     * build new instance of almadallah client, with excute login.
     *
     * @param account
     * @return
     * @throws Exception
     */
    public static ALMADALLAHClient getInstance(Account account) throws Exception {
        ALMADALLAHClient instance = new ALMADALLAHClient(account);
        instance.login();
        return instance;
    }

    /**
     * @author AKhalifa
     * login function for almadallah client
     */
    @Override
    public boolean login() {
        long mi = new Date().getTime();
        boolean loginState = false;

        Logger.getLogger(ALMADALLAHClient.class.getName()).log(Level.INFO, new Date() + "{0}...** Start login Almadallah Client ...");
        try {
            this.driver.navigate().to(LOGIN_URL);
            disableCss();
            WebDriverWait wait = new WebDriverWait(this.driver, 10);
            // Thread.sleep(2000);
            wait.until(ExpectedConditions.visibilityOf(this.driver.findElement(By.id("over"))));

            if (this.driver.findElements(By.id("ctl00_contDefaultMaster_rtbUserName")).size() != 0 && this.driver.findElements(By.id("ctl00_contDefaultMaster_rtbPassword")).size() != 0 && this.driver.findElements(By.id("ctl00_contDefaultMaster_rbLogin")).size() != 0) {
                this.webService.login(this.driver, "", "ctl00_contDefaultMaster_rtbUserName", account.getUser(), "ctl00_contDefaultMaster_rtbPassword", account.getPass(), 1, "ctl00_contDefaultMaster_rbLogin", 3000);

                //check if error message is still render if not the site login successfully.
                if (this.driver.findElements(By.id("contDefaultMaster_lblErrorMessage")).size() == 0) {
                    loginState = true;
//                    this.driver.navigate().to(CARD_URL);
//                    disableCss();
                } else {
                    WebElement loginErrorMessage = this.driver.findElement(By.id("contDefaultMaster_lblErrorMessage"));

                    if (loginErrorMessage.getText().equals("Incorrect UserName or Password")) {
                        loginState = false;
                        Logger.getLogger(ALMADALLAHClient.class.getName()).log(Level.INFO, new Date() + "{0}...** login Almadallah Client is  Incorrect UserName or Password...");
//                        this.driver.quit();
                    } else if (loginErrorMessage.getText().equals("Please enter valid UserName & Password")) {
                        loginState = false;
                        Logger.getLogger(ALMADALLAHClient.class.getName()).log(Level.INFO, new Date() + "{0}...** login Almadallah Client is  Please enter valid UserName & Password...");
//                        this.driver.quit();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(ALMADALLAHClient.class.getName()).log(Level.INFO, new Date() + "Almadallah unknown login error");
//            this.driver.quit();
        }
//        this.driver.quit();
        return loginState;
    }

    /***
     * @author AKhalifa
     * @param eId
     * @return Insurance card Object that handle the response of request.
     */
    @Override
    public InsuranceCard getCardByEId(String eId) {
        InsuranceCard card = null;
        Logger.getLogger(ALMADALLAHClient.class.getName()).log(Level.INFO, new Date() + " *start fetching Almadallah card information");
        Long mi = new Date().getTime();
        try {
            refresh(CARD_URL);
            // disableCss();

//            WebDriverWait wait = new WebDriverWait(this.driver, 10);
//            wait.until(ExpectedConditions.visibilityOf(this.driver.findElement(By.id("ctl00_contDefaultMaster_rtbMemberCardNoOrEmiratesIDNo"))));

            if (this.driver.findElements(By.id("ctl00_contDefaultMaster_rtbMemberCardNoOrEmiratesIDNo")).size() != 0 && this.driver.findElements(By.id("ctl00_contDefaultMaster_rbtSearch")).size() != 0) {
                WebElement eIdTarget = this.driver.findElement(By.id("ctl00_contDefaultMaster_rtbMemberCardNoOrEmiratesIDNo"));
                eIdTarget.clear();
                eIdTarget.sendKeys(eId);
                this.driver.findElement(By.id("ctl00_contDefaultMaster_rbtSearch")).click();
                Thread.sleep(5000);
                if (this.driver.findElements(By.id("contDefaultMaster_lblMessage")).size() != 0 && this.driver.findElement(By.id("contDefaultMaster_lblMessage")).getText().equals("Not a valid Member Card No / Emirates ID No")) {
                    card = new InsuranceCard("Not a valid Member Card No / Emirates ID No", -2);
                    Logger.getLogger(ALMADALLAHClient.class.getName()).log(Level.INFO, "{0} ,...** Finish fetching Almadallah Client ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                } else {
                    if (driver.findElements(By.id("contDefaultMaster_lblMemberDetails")).size() != 0) {
                        card = new InsuranceCard();
                        WebElement mainFetchingElement = this.driver.findElement(By.xpath("//*[@id=\"contDefaultMaster_divMemberInfo\"]/div/div[2]"));
                        List<WebElement> personalInfo = mainFetchingElement.findElements(By.tagName("dd"));

                        card.setPayer(personalInfo.get(0).getText());
                        card.setMemberId(personalInfo.get(1).getText());
                        card.setName(personalInfo.get(3).getText());
                        card.setDateOfBirth(personalInfo.get(4).getText());
                        card.setGender(personalInfo.get(6).getText());
                        card.setStatus(personalInfo.get(10).getText());
                        card.setPlan(personalInfo.get(13).getText());

                        String copay = "Maternity:[" + personalInfo.get(14).getText() + "],";
                        copay = copay + "Dental:[" + personalInfo.get(15).getText() + "],";
                        copay = copay + "Optical:[" + personalInfo.get(16).getText() + "],";
                        copay = copay + "Copay:[" + personalInfo.get(17).getText() + "],";
                        copay = copay + "Deductable:[" + personalInfo.get(18).getText() + "],";

                        card.setCopay(copay);
                        card.setExpiryDate(personalInfo.get(19).getText());
                        card.setMessage(personalInfo.get(20).getText());

                        card.setSuccess(1);
                        Logger.getLogger(ALMADALLAHClient.class.getName()).log(Level.SEVERE, "{0} , Finish fetching card Almadallah CLient ... Time(s):{1} , Name = {2}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000, card.getName()});
                    }
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            card = new InsuranceCard("Almadallah unknown error", -1);
        }
       // this.driver.quit();
        return card;
    }

    @Override
    public InsuranceCard getCardByEId(String eId, String encType) {
        return getCardByEId(eId);
    }

    /**
     * check if site eligible for check Emirates id.
     *
     * @return true if applicable otherwise false
     */
    @Override
    public boolean isValid() throws InterruptedException {
        this.driver.navigate().to(CARD_URL);
        if (this.driver.getCurrentUrl().equals(LOGIN_URL)) {
            if (login())
                return true;
            return false;
        }
        return true;
    }

    @Override
    public void destroyDrive() {
        this.driver.quit();
    }

}
