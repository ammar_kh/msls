package com.accumed.msls.payer;



import com.accumed.msls.model.Account;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author AKhalifa
 * class for implements login, fetch almadallah inscurance card information for client
 */
public class NextCareClient extends Client {

    private static final String LOGIN_URL = "https://pulse-uae.tatsh.com/Login2.aspx?s=2";
    private static final String TEMP_URL = "https://pulse-uae.tatsh.com/ClaimRegistration2.aspx";
    private static final String CARD_URL = "https://pulse-uae.tatsh.com/EligibilityChecking2.aspx";

    public NextCareClient() {
        this(null);
    }

    public NextCareClient(Account account) {
        super(account);
    }

    public static NextCareClient getInstance(Account account) throws Exception {
        NextCareClient instance = new NextCareClient(account);
        instance.login();
        return instance;
    }
    public boolean login() {
        long mi = new Date().getTime();
        boolean loginState = false;
        Logger.getLogger(NextCareClient.class.getName()).log(Level.INFO, "start login NextCare client {0}", new Date());
        try {
            this.driver.navigate().to(LOGIN_URL);
            this.webService.login(this.driver, "", "txtUserName", account.getUser(), "txtPassword", account.getPass(), 1, "Button1", 1000);
            if (this.driver.findElements(By.id("lblerrLogin")).size() != 0) {
                if (this.driver.findElement(By.id("lblerrLogin")).getText().equals("invalid userName or password")) {
                    Logger.getLogger(NextCareClient.class.getName()).log(Level.INFO, "finish login NextCare client invalid userName or password : {0}", new Date());
                    loginState = false;
                    this.driver.quit();
                }
            } else if (this.driver.getCurrentUrl().equals(TEMP_URL)) {
                //check if system login in successfully then move to eligibility page.
                this.driver.navigate().to(CARD_URL);
                Logger.getLogger(NextCareClient.class.getName()).log(Level.INFO, "Finish Login NextCare Client : {0}", new Date());
                loginState = true;
            }
        } catch (Exception ex) {
            Logger.getLogger(NextCareClient.class.getName()).log(Level.INFO, "NextCare Login unknow error : {0}", new Date());
            this.driver.quit();
        }
        // checkIfLogin();
        return loginState;
    }

    @Override
    public InsuranceCard getCardByEId(String eId) {
        return null;
    }

    @Override
    public InsuranceCard getCardByEId(String eId, String encType) {
        InsuranceCard card = null;
        long mi = new Date().getTime();
        Logger.getLogger(NextCareClient.class.getName()).log(Level.INFO, "start fetching NextCare Client info : {0}", new Date());
        try {
            // check if eligibility page is open .
            if (this.driver.getCurrentUrl().equals(CARD_URL)) {

                this.driver.findElement(By.xpath("//*[@id=\"eligibilityCheckForm\"]/div/div/div/div[2]/div/div/div[1]/div/ul/div/label[3]")).click();
                this.driver.findElement(By.id("txtIDTypeValue")).sendKeys(eId);
                Thread.sleep(2000);
                //get hidden list element .
                Actions action = new Actions(this.driver);
                action.moveToElement(this.driver.findElement(By.id("ctl00_ContentPlaceHolderBody_cmbType_chosen"))).perform();
                this.driver.findElement(By.id("ctl00_ContentPlaceHolderBody_cmbType_chosen")).click();

                List<WebElement> listElements = this.driver.findElement(By.id("ctl00_ContentPlaceHolderBody_cmbType_chosen")).findElements(By.tagName("li"));

                if (encType.equals("IP")) {
                    listElements.get(0).click();
                } else {
                    listElements.get(1).click();
                }
                this.driver.findElement(By.id("btnCheckEligibilityorSearchbyPolicy")).click();
                Thread.sleep(5000);
                //result of eligibility.
                // wait until the page has fetch the result of eligibility.
                new WebDriverWait(this.driver, 10)
                        .until(ExpectedConditions.visibilityOf(this.driver.findElement(By.id("eligibilityCheckForm"))));

                if (this.driver.findElements(By.id("eligibilityCheckForm")).size() != 0) {
                    // wait until object render inside form
                    Thread.sleep(2000);
                    if (this.driver.findElements(By.id("lblResultMessage2")).size() != 0) {

                        String lblMessage1 = this.driver.findElement(By.id("lblResultMessage1")).getText();
                        String lblMessage2 = this.driver.findElement(By.id("lblResultMessage2")).getText();

                        if (lblMessage2.equals("This beneficiary is Invalid")) {
                            card = new InsuranceCard("This beneficiary is Invalid", -2);
                            Logger.getLogger(NextCareClient.class.getName()).log(Level.INFO, "finish fetching NextCare client information This beneficiary is Invalid : {0}", new Date());
                            this.driver.quit();
                        } else if (lblMessage2.isEmpty() && !lblMessage1.isEmpty()) {
                            // eligible
                            card = new InsuranceCard();
                            System.out.println("phase 1");
                            card.setMessage(this.driver.findElement(By.id("lblResultMessage1")).getText());
                            System.out.println("phase 2");
                            card.setName(this.driver.findElement(By.id("lblPatientName")).getText());
                            System.out.println("phase 3");
                            card.setMemberId(this.driver.findElement(By.id("lblCardNumber")).getText());
                            System.out.println("phase 4");
                            card.setGender(this.driver.findElement(By.id("lblGender")).getText());
                            System.out.println("phase 5");
                            card.setDateOfBirth(this.driver.findElement(By.id("lblDOB")).getText());
                            System.out.println("phase 6");
                            card.setPayer(this.driver.findElement(By.id("lblPayer")).getText());
                            System.out.println("phase 7");
                            card.setExpiryDate(this.driver.findElement(By.id("lblValidityBetween")).getText());
                            System.out.println("phase 4");
                            card.setPlan(this.driver.findElement(By.id("lblCategroy")).getText());

                            List<WebElement> benefit1 = this.driver.findElement(By.id("divResultRelevantMOB")).findElements(By.tagName("div"));
                            List<WebElement> benefit2 = this.driver.findElement(By.cssSelector("div[class='col-xs-12 border-eligib']")).findElements(By.tagName("div"));
                            Thread.sleep(2000);
                            String copay = "";
                            if (benefit1.size() > 0)
                                for (WebElement div : benefit1) {
                                    copay += div.getText().trim().replace("\r", "").replace("\t", "").replace(":\n", ": ").replace("\n\n", "\n") + "\n";
                                }
                            if (benefit2.size() > 0)
                                for (WebElement div : benefit2) {
                                    copay += div.getText().trim().replace("\r", "").replace("\t", "").replace(":\n", ": ").replace("\n\n", "\n") + "\n";
                                }
                            card.setCopay(copay);
                            card.setSuccess(1);
                            Logger.getLogger(NextCareClient.class.getName()).log(Level.INFO, "date{0} , Finish fetching card Nextcare CLient ... Time(s):{1} , Name = {2}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000, card.getName()});
                        }
                    }
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            card = new InsuranceCard(" finish fetching NextCare unknown error", -1);
        }
        //this.driver.close();
        return card;
    }

    @Override
    public boolean isValid() throws InterruptedException {
        this.driver.navigate().to(CARD_URL);
        if (this.driver.getCurrentUrl().equals(LOGIN_URL)) {
            if (login())
                return true;
            return false;
        }
        return true;
    }

    @Override
    public void destroyDrive() {
        this.driver.quit();
    }


    public boolean checkIfLogin() {
        String ASPNET_SessionId = this.driver.manage().getCookieNamed("ASP.NET_SessionId").toString();
        return false;
    }
}
