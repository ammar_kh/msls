package com.accumed.msls.payer;



import com.accumed.msls.model.Account;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author AKhalifa
 * core class for thiqa client that whill excute login , get insaurance card info by eId.
 * all Rights Reserved for UAE ACCUMED 2009 - 2020
 */
public class THIQAClient extends Client {

    private static final String LOGIN_URL = "https://www.thiqa.ae/thiqaApp/loadProviderLoginForm.action";
    private static final String CARD_URL = "https://www.thiqa.ae/thiqaApp/loadMemberExtnSearch.action";

    public THIQAClient() {
        this(null);
    }

    public THIQAClient(Account account) {
        super(account);
    }

    public static THIQAClient getInstance(Account account) throws Exception {
        THIQAClient instance = new THIQAClient(account);
        instance.login();
        return instance;
    }
    /**
     * @return loginState as boolean data type / true if can login , false if cannot access to system.
     * @author AKhalifa
     */
    @Override
    public boolean login() {
        long mi = new Date().getTime();
        boolean loginState = false;

        Logger.getLogger(THIQAClient.class.getName()).log(Level.INFO, new Date() + "{0}...** Start login Thiqa Client ...");
        try {

            this.webService.login(this.driver, LOGIN_URL, "pdrUserName", account.getUser(), "pdrPassword", account.getPass(), 3, "//*[@id=\"pdrLoginForm\"]/div/div/div[5]/div/div/button", 0);

            if (this.driver.findElements(By.id("pdrLoginError")).size() != 0) {
                if (this.driver.findElement(By.id("pdrLoginError")).getText().equals("Incorrect provider username or password.")) {
                    Logger.getLogger(THIQAClient.class.getName()).log(Level.INFO, new Date() + "{0}...** login Thiqa Client is Incorrect provider username or password....");
                } else if (this.driver.findElement(By.id("pdrLoginError")).getText().equals("Please enter a valid user name and password"))
                    Logger.getLogger(THIQAClient.class.getName()).log(Level.INFO, new Date() + "{0}...** login Thiqa Client is Please enter a valid user name and password....");
                loginState = false;
            }
            Logger.getLogger(THIQAClient.class.getName()).log(Level.INFO, new Date() + "{0}...** Finish login Thiqa Client ...");
            loginState = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return loginState;
    }

    /**
     * @param eId
     * @return Insurance Card Object
     * @author AKhalifa
     */
    @Override
    public InsuranceCard getCardByEId(String eId) throws InterruptedException {
        InsuranceCard card = null;
        Logger.getLogger(THIQAClient.class.getName()).log(Level.INFO, new Date() + " *start fetching Thiqa card information");
        long mi = new Date().getTime();
        try {
            this.driver.navigate().to(CARD_URL);
            WebDriverWait wait = new WebDriverWait(this.driver, 10);
            wait.until(ExpectedConditions.visibilityOf(this.driver.findElement(By.id("eid1"))));
            if (this.driver.findElements(By.id("eid1")).size() != 0 && this.driver.findElements(By.id("eid2")).size() != 0 && this.driver.findElements(By.id("eid3")).size() != 0 && this.driver.findElements(By.id("eid4")).size() != 0) {
                this.driver.findElement(By.id("eid2")).sendKeys(eId.substring(4, 8));
                this.driver.findElement(By.id("eid3")).sendKeys(eId.substring(9, 16));
                this.driver.findElement(By.id("eid4")).sendKeys(eId.substring(eId.length() - 1));
                this.driver.findElement(By.name("login")).click();
                this.driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
                if (this.driver.findElements(By.className("notification_error")).size() != 0) {

                    if (this.driver.findElement(By.className("notification_msg")).getText().equals("The Emirates ID number entered is incorrect. Please check and try again.")) {
                        card = new InsuranceCard("The Emirates ID number entered is incorrect. Please check and try again.", -2);
                        Logger.getLogger(THIQAClient.class.getName()).log(Level.INFO, "{0} ,...** Finish fetching Thiqa Client ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                    } else if (this.driver.findElement(By.className("notification_msg")).getText().contains("\"Not Eligible\"")) {
                        card = new InsuranceCard("Status: \"Not Eligible\", This Emirates ID number is not in our records", -2);
                        Logger.getLogger(THIQAClient.class.getName()).log(Level.INFO, "{0} ,...** Finish fetching Thiqa Client ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                    } else if (this.driver.findElement(By.className("notification_msg")).getText().equals("Our electronic services are currently down. Please try again later.")) {
                        card = new InsuranceCard("Our electronic services are currently down. Please try again later.", -2);
                        Logger.getLogger(THIQAClient.class.getName()).log(Level.INFO, "{0} ,...** Finish fetching Thiqa Client ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                    } else if (this.driver.findElement(By.className("notification_msg")).getText().equals("Please enter either the Emirates ID number or the Thiqa card number."))
                        getCardByEId(eId);
                } else if (this.driver.findElements(By.className("notification_msg")).size() != 0) {
                    if (this.driver.findElement(By.className("notification_msg")).getText().equals("Card is Valid")) {
                        card = new InsuranceCard();
                        WebElement cardContent = this.driver.findElement(By.className("cardcontent"));
                        List<WebElement> cardElement = cardContent.findElements(By.className("cardvalue"));
                        List<WebElement> cardElementLabels = cardContent.findElements(By.className("cardlabel"));

                        card.setGender(cardElement.get(0).getText());
                        card.setStartDate(cardElement.get(1).getText());
                        card.setExpiryDate(cardElement.get(2).getText());
                        card.setName(this.driver.findElement(By.className("memname")).getText());
                        card.setBenefits(cardElement.get(3).getText());

                        String copay = "";

                        if (cardElement.size() != 0 && cardElement.size() > 4) {
                            for (int i = 4; i < cardElement.size(); i++) {
                                copay += "[ " + cardElementLabels.get(i).getText() + " ] : [ " + cardElement.get(i).getText() + " ],\n";
                            }
                            copay = copay.substring(0, copay.length() - 2) + "\n";
                        }
                        System.out.println(copay);
                        card.setCopay(copay);

                        card.setSuccess(1);
                        Logger.getLogger(THIQAClient.class.getName()).log(Level.INFO, "{0} ,...** Finish fetching Thiqa Client ... Time(s):{1}", new Object[]{new Date(), (new Date().getTime() - mi) / 1000});
                        this.driver.navigate().refresh();
                    }
                }

            } else {
                getCardByEId(eId);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            card = new InsuranceCard("Thiqa unknown error!", -1);
            isValid();
        }
        //this.driver.close();
        return card;
    }

    @Override
    public InsuranceCard getCardByEId(String eId, String encType) throws InterruptedException {
        return getCardByEId(eId);
    }

    @Override
    public boolean isValid() throws InterruptedException {
        this.driver.navigate().to(CARD_URL);
        if (this.driver.getCurrentUrl().equals(LOGIN_URL)) {
            if (login())
                return true;
            return false;
        }
        return true;
    }

    @Override
    public void destroyDrive() {
        this.driver.quit();
    }


}
