/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.pools.adnic;

import com.accumed.msls.model.Account;
import com.accumed.msls.payer.ADNICClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 *
 * @author bmahow
 */
public class ADNICFactory extends BasePooledObjectFactory<ADNICClient> {

    Account account;

    public ADNICFactory(Account account) {
        super();
        this.account = account;
    }
    

    @Override
    public ADNICClient create() throws Exception {
        return ADNICClient.getInstance(this.account);  //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Use the default PooledObject implementation.
     *
     * @param buffer
     * @return
     */

    @Override
    public PooledObject<ADNICClient> wrap(ADNICClient buffer) {
        return new DefaultPooledObject<>(buffer); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * When an object is returned to the pool, clear the buffer.
     *
     * @param pooledObject
     */
    @Override
    public void passivateObject(PooledObject<ADNICClient> pooledObject) {
        //pooledObject.getObject().reset(); --zeroing object without re-create
    }

    // for all other methods, the no-op implementation
    // in BasePooledObjectFactory will suffice
    @Override
    public void activateObject(PooledObject<ADNICClient> p) throws Exception {
        super.activateObject(p); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean validateObject(PooledObject<ADNICClient> p) {
        boolean ret = true;
        Logger.getLogger(ADNICFactory.class.getName()).
                log(Level.INFO, "id:{0}::validateObject ....",
                        new Object[]{p.getObject().getUniqueId()});
        try {
            ret = p.getObject().isValid();
        } catch (Exception ex) {
            Logger.getLogger(ADNICFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
//        if (p.getObject().getRepoTimeStamp().before(cachedRepositoryService.getRepo().getTimeStamp())) {
//            ret = false;
//        }
//        if (ret && p.getObject().isNewPackageExisted()) {
//            ret = false;
//        }
//        if(ret && p.getObject().isDirty()){
//            Logger.getLogger(ALMADALLAHFactory.class.getName()).
//                    log(Level.INFO, "id'{'{0}'}'::worker is dirty.", p.getObject().getUniqueId());
//            ret = false;
//        }
//        if(ret){
//            ret = super.validateObject(p); //To change body of generated methods, choose Tools | Templates.
//        }
        Logger.getLogger(ADNICFactory.class.getName()).
                log(Level.INFO, "id:{0}::validateObject completed {1}",
                        new Object[]{p.getObject().getUniqueId(), ret ? "---valid---" : "---invalid---"});
        return ret;
    }

    @Override
    public void destroyObject(PooledObject<ADNICClient> p) throws Exception {
        p.getObject().destroyDrive();
        super.destroyObject(p); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PooledObject<ADNICClient> makeObject() throws Exception {
        return super.makeObject(); //To change body of generated methods, choose Tools | Templates.
    }

}
