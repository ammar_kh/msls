/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.pools.almadallah;


import java.util.logging.Level;
import java.util.logging.Logger;

import com.accumed.msls.model.Account;
import com.accumed.msls.payer.ALMADALLAHClient;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 *
 * @author smutlak
 */
public class ALMADALLAHFactory extends BasePooledObjectFactory<ALMADALLAHClient> {

    private Account account;
    public ALMADALLAHFactory(Account account) {
        super();
        this.account = account;
    }


    @Override
    public ALMADALLAHClient create() throws Exception {
        return ALMADALLAHClient.getInstance(account);
    }

    /**
     * Use the default PooledObject implementation.
     *
     * @param buffer
     * @return
     */
    @Override
    public PooledObject<ALMADALLAHClient> wrap(ALMADALLAHClient buffer) {
        return new DefaultPooledObject<>(buffer);
    }

    /**
     * When an object is returned to the pool, clear the buffer.
     *
     * @param pooledObject
     */
    @Override
    public void passivateObject(PooledObject<ALMADALLAHClient> pooledObject) {
        //pooledObject.getObject().reset(); --zeroing object without re-create
    }

    // for all other methods, the no-op implementation
    // in BasePooledObjectFactory will suffice
    @Override
    public void activateObject(PooledObject<ALMADALLAHClient> p) throws Exception {
        super.activateObject(p); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean validateObject(PooledObject<ALMADALLAHClient> p) {
        boolean ret = true;
        Logger.getLogger(ALMADALLAHFactory.class.getName()).
                log(Level.INFO, "id:{0}::validateObject ....",
                        new Object[]{p.getObject().getUniqueId()});
        try {
            ret = p.getObject().isValid();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        if (p.getObject().getRepoTimeStamp().before(cachedRepositoryService.getRepo().getTimeStamp())) {
//            ret = false;
//        }
//        if (ret && p.getObject().isNewPackageExisted()) {
//            ret = false;
//        }
//        if(ret && p.getObject().isDirty()){
//            Logger.getLogger(ALMADALLAHFactory.class.getName()).
//                    log(Level.INFO, "id'{'{0}'}'::worker is dirty.", p.getObject().getUniqueId());
//            ret = false;
//        }
//        if(ret){
//            ret = super.validateObject(p); //To change body of generated methods, choose Tools | Templates.
//        }
        Logger.getLogger(ALMADALLAHFactory.class.getName()).
                log(Level.INFO, "id:{0}::validateObject completed {1}",
                        new Object[]{p.getObject().getUniqueId(), ret?"---valid---":"---invalid---"});
        return ret;
    }

    @Override
    public void destroyObject(PooledObject<ALMADALLAHClient> p) throws Exception {
        p.getObject().destroyDrive();
        super.destroyObject(p); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PooledObject<ALMADALLAHClient> makeObject() throws Exception {
        return super.makeObject(); //To change body of generated methods, choose Tools | Templates.
    }
    
    

}
