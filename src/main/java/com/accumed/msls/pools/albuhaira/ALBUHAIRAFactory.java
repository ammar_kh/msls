/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.pools.albuhaira;

import com.accumed.msls.model.Account;
import com.accumed.msls.payer.AlBuhairaClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 *
 * @author bmahow
 */
public class ALBUHAIRAFactory extends BasePooledObjectFactory<AlBuhairaClient> {
    Account account;

    public ALBUHAIRAFactory(Account account) {
        super();
        this.account = account;
    }
    
    @Override
    public AlBuhairaClient create() throws Exception {
        return AlBuhairaClient.getInstance(this.account);
    }

    /**
     * Use the default PooledObject implementation.
     *
     * @param buffer
     * @return
     */
    @Override
    public PooledObject<AlBuhairaClient> wrap(AlBuhairaClient buffer) {
        return new DefaultPooledObject<>(buffer);
    }

    /**
     * When an object is returned to the pool, clear the buffer.
     *
     * @param pooledObject
     */
    @Override
    public void passivateObject(PooledObject<AlBuhairaClient> pooledObject) {
        //pooledObject.getObject().reset(); --zeroing object without re-create
    }

    // for all other methods, the no-op implementation
    // in BasePooledObjectFactory will suffice
    @Override
    public void activateObject(PooledObject<AlBuhairaClient> p) throws Exception {
        super.activateObject(p); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean validateObject(PooledObject<AlBuhairaClient> p) {
        boolean ret = true;
        Logger.getLogger(ALBUHAIRAFactory.class.getName()).
                log(Level.INFO, "id:{0}::validateObject ....",
                        new Object[]{p.getObject().getUniqueId()});
        try {
            ret = p.getObject().isValid();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        if (p.getObject().getRepoTimeStamp().before(cachedRepositoryService.getRepo().getTimeStamp())) {
//            ret = false;
//        }
//        if (ret && p.getObject().isNewPackageExisted()) {
//            ret = false;
//        }
//        if(ret && p.getObject().isDirty()){
//            Logger.getLogger(ALMADALLAHFactory.class.getName()).
//                    log(Level.INFO, "id'{'{0}'}'::worker is dirty.", p.getObject().getUniqueId());
//            ret = false;
//        }
//        if(ret){
//            ret = super.validateObject(p); //To change body of generated methods, choose Tools | Templates.
//        }
        Logger.getLogger(ALBUHAIRAFactory.class.getName()).
                log(Level.INFO, "id:{0}::validateObject completed {1}",
                        new Object[]{p.getObject().getUniqueId(), ret?"---valid---":"---invalid---"});
        return ret;
    }

    @Override
    public void destroyObject(PooledObject<AlBuhairaClient> p) throws Exception {
        p.getObject().destroyDrive();
        super.destroyObject(p); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PooledObject<AlBuhairaClient> makeObject() throws Exception {
        return super.makeObject(); //To change body of generated methods, choose Tools | Templates.
    }

 
}
