/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accumed.msls.pools.daman;

import com.accumed.msls.model.Account;
import com.accumed.msls.payer.DAMANClient;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 * @author smutlak
 */
public class DAMANFactory extends BasePooledObjectFactory<DAMANClient> {

    private Account account;

    public DAMANFactory(Account account) {
        super();
        this.account = account;

    }


    @Override
    public DAMANClient create() throws Exception {
        return DAMANClient.getInstance(this.account);
    }

    /**
     * Use the default PooledObject implementation.
     *
     * @param buffer
     * @return
     */
    @Override
    public PooledObject<DAMANClient> wrap(DAMANClient buffer) {
        return new DefaultPooledObject<>(buffer);
    }

    /**
     * When an object is returned to the pool, clear the buffer.
     *
     * @param pooledObject
     */
    @Override
    public void passivateObject(PooledObject<DAMANClient> pooledObject) {
        //pooledObject.getObject().reset(); --zeroing object without re-create
    }

    // for all other methods, the no-op implementation
    // in BasePooledObjectFactory will suffice
    @Override
    public void activateObject(PooledObject<DAMANClient> p) throws Exception {
        super.activateObject(p); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean validateObject(PooledObject<DAMANClient> p) {
        boolean ret = true;
        Logger.getLogger(DAMANFactory.class.getName()).
                log(Level.INFO, "id:{0}::validateObject ....",
                        new Object[]{p.getObject().getUniqueId()});
        try {
            ret = p.getObject().isValid();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Logger.getLogger(DAMANFactory.class.getName()).
                log(Level.INFO, "id:{0}::validateObject completed {1}",
                        new Object[]{p.getObject().getUniqueId(), ret ? "---valid---" : "---invalid---"});
        return ret;
    }

    @Override
    public void destroyObject(PooledObject<DAMANClient> p) throws Exception {
        p.getObject().destroyDrive();
        super.destroyObject(p); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PooledObject<DAMANClient> makeObject() throws Exception {
        return super.makeObject(); //To change body of generated methods, choose Tools | Templates.
    }


}
